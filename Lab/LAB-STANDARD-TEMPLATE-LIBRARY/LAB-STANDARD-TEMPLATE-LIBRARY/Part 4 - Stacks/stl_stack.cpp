// Lab - Standard Template Library - Part 4 - Stacks
// Matt Carpenter

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
	string temp;
	stack<string> letter;
	cout << "Enter a word one letter at a time, or type UNDO to remove the last letter or DONE to output: ";
	while (true)
	{
		cin >> temp;
		if (temp == "UNDO")
		{
			if (letter.empty())
				cout << "cannot undo empty stack" << endl;
			else
				letter.pop();
		}
		else if (temp == "DONE")
		{
			while (!letter.empty())
			{
				cout << letter.top();
				letter.pop();
			}
		}
		else
			letter.push(temp);

	}
	
	
    cin.ignore();
    cin.get();
    return 0;
}
