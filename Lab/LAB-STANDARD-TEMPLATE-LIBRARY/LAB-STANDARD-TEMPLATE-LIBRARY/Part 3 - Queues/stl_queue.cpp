// Lab - Standard Template Library - Part 3 - Queues
// Matt Carpenter

#include <iostream>
#include <string>
#include <queue>
using namespace std;
void displayTotal(queue<float>& account);

int main()
{
	float new_transaction;
	queue<float> transaction;
	
	while (true)
	{
		
		cout << "Enter a transaction amount, to quit enter 0: ";
		cin >> new_transaction;
		if (new_transaction != 0)
		{
			transaction.push(new_transaction);
		}
		else if (new_transaction == 0)
			displayTotal(transaction);
		
	}
    cin.ignore();
    cin.get();
    return 0;
}
void displayTotal(queue<float>& account)
{
	float total = 0;
	cout << "Transaction History: " << endl;
	if (!account.empty())
	{
		for(auto i = account.size(); i>0;i--)
		{
			cout << i << ". " << account.front() << endl;
			total += account.front();
			account.pop();
		}
		
		
	}
	cout << "Account balance = " << total << endl;
}
