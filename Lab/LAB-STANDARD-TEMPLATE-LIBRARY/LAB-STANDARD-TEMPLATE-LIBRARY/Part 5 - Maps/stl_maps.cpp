// Lab - Standard Template Library - Part 5 - Maps
// Matt Carpenter

#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
	map<char, string> colors;
	colors['r'] = "FF0000";
	colors['g'] = "00FF00";
	colors['b'] = "0000FF";
	colors['c'] = "00FFFF";
	colors['m'] = "FF00FF";
	colors['y'] = "FFFF00";
	

	bool done = false;
	while (done != true)
	{
		char color;
		cout << "Enter the first letter of a color: ";
		cin.get(color);
		if (color == 'q')
			done = true;
		else
		cout << "Hex: " << colors[color] << endl;
		cin.ignore();


	}
    cin.ignore();
    cin.get();
    return 0;
}
