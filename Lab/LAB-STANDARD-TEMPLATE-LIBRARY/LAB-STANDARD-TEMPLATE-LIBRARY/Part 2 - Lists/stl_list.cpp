// Lab - Standard Template Library - Part 2 - Lists
// Matt Carpenter

#include <iostream>
#include <list>
#include <string>
using namespace std;
void DisplayList(list<string>& state);

int main()
{
	list<string> state;
	while (true)
	{
		cout << endl << "MAIN MENU" << endl;
		cout << "1. Add new state to front of list. " << endl //push_front
			<< "2. Add new state to back of list. " << endl //push_back
			<< "3. Remove a state from the front of the list. " << endl //pop_front
			<< "4. Remove a state from the back of the list. " << endl // pop_back
			<< "5. Continue. " << endl;
		cout << "Enter your choice: ";
		int choice;
		cin >> choice;
		string temp_state_name = "";
		switch(choice)
		{
		case 1:
			
			cout << "Enter a state name: ";
			cin >> temp_state_name;
			state.push_front(temp_state_name);
			break;
		case 2:
			
			cout << "Enter a state name: ";
			cin >> temp_state_name;
			state.push_back(temp_state_name);
			break;
		case 3:
			state.pop_front();
			break;
		case 4:
			state.pop_back();
			break;
		case 5:
			DisplayList(state);
			state.reverse();
			DisplayList(state);
			state.sort();
			DisplayList(state);
			state.reverse();
			DisplayList(state);
			break;
		case 6:
			cout << "exiting.";
			exit(1);
		default:
			cout << "Invalid choice." << endl;
		}
		
		
		
	}
    cin.ignore();
    cin.get();
    return 0;
}
void DisplayList(list<string>& state)
{
	for (list<string>::iterator it = state.begin();
		it != state.end();
		it++)
	{
		cout << *it << "\t";
	}
	cout << endl;

}