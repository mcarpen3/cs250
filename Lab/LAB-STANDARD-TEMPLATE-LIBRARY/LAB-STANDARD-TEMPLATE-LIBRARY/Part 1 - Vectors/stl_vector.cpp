// Lab - Standard Template Library - Part 1 - Vectors
// Matt Carpenter

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
	vector<string> courses;


	bool done = false;
	while (done == false)
	{
		cout << endl << "MAIN MENU" << endl;
		cout << "Vector Size = " << courses.size() << endl;
		cout << "1. Add a new course " <<
			"2. Remove last course "
			<< "3. Display course list "
			<< "4. Quit " << endl;
		int choice;
		cin >> choice;
		if (choice == 1)
		{
			string newCourse;
			cout << "What is the course name? ";
			cin >> newCourse;
			courses.push_back(newCourse);
			//add a new course	
		}
		else if (choice == 2)
		{
			courses.pop_back();
			
			//Remove last course
		}
		else if (choice == 3)
		{
			for (int i = 0; i < courses.size(); i++)
			{
				cout << i << ". " << courses[i] << endl;
			}
			//Display course list
		}
		else if (choice == 4)
		{

			done = true;	//quit
		}
		else
			break;

	}
    cin.ignore();
    cin.get();
    return 0;
}
