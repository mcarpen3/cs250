\input{BASE-HEAD}
\newcommand{\laClass}       {CS 250}
\newcommand{\laSemester}    {Spring 2018}
\newcommand{\laChapter}     {}
\newcommand{\laType}        {Lab}
\newcommand{\laAssignment}  {1}
\newcommand{\laPoints}      {5}
\newcommand{\laTitle}       {Exception Handling}
\newcommand{\laStarterFiles}{Download from GitHub.}
\newcommand{\laTopics}      {Exception handling}
\setcounter{chapter}{1}
\setcounter{section}{1}
\addtocounter{section}{-1}
\newcounter{question}
\toggletrue{answerkey}
\togglefalse{answerkey}
\input{BASE-HEADER}

\input{INSTRUCTIONS-LAB}

    \section{About}

        Exception handling helps us design better code that is intended
        to be reused over and over. When creating a data structure
        that other programmers will be using, it can be important to
        report errors with your structure to them so that they can decide
        how to resolve the error; you can't always make the best design
        decisions on your own, it's up to each programmer to figure out
        the needs for their project.

        You will start with the following code, which contains
        an object that handles file streams, as well as the main
        source file that contains tests.
        At the moment, it should compile and run, but it will crash
        during the tests because the RecordManager object doesn't
        have error checking.

        In this lab, you will implement the exception handling
        within the RecordManager, as well as checking for exceptions
        from the main file.


    \subsection{Setting up the project}

        Download the starter zip file, \texttt{LAB-EXCEPTION-HANDLING.zip},
        off GitHub. This contains:

        \begin{itemize}
            \item   \texttt{RecordManager.hpp}
            \item   \texttt{main.cpp}
        \end{itemize}

    \newpage
    
% -------------------------------------------------------------------------------------------- %
    \section{Lab specifications}
    

        \subsection{Adding error checking}

            Within the RecordManager class functions, you will add error
            checking and \texttt{throw} commands. Outside the class, in
            main and the tests, you will add the \texttt{try/catch} blocks.
            
            \begin{intro}{Throw, try, and catch}
                \textbf{INSIDE} a function is where the
                \texttt{throw} command is used. After checking
                for some error state, the response is to
                \texttt{throw} the type of exception that has
                occurred, as well as an error message. 
                ~\\~\\
                \textbf{OUTSIDE} a function (at the function-call level)
                is where \texttt{try} and \texttt{catch} commands
                are used. When a function that may throw an exception is
                being called, it should be wrapped within a \texttt{try}
                block. Then, the \texttt{catch} block(s) follow, to catch
                different types of exceptions, resolve them, clean up,
                and allow the program to continue.
            \end{intro}

        \subsubsection*{Updating RecordManager::OpenOutputFile}
        In this function, add an error check before
        \texttt{m\_outputs[ index ]} is accessed: Check to see
        if the \textit{index} value is equal to -1. If so, throw
        a \texttt{runtime\_error} like this:

\begin{lstlisting}[style=code]
if ( index == -1 )
{
    throw runtime_error( "No available files" );
}    
\end{lstlisting}

        \subsubsection*{Updating RecordManager::CloseOutputFile}
        This function should also throw a \texttt{runtime\_error} if
        the returned index is -1.

        \subsubsection*{Updating RecordManager::WriteToFile}
        This function should also throw a \texttt{runtime\_error} if
        the returned index is -1.

        \subsubsection*{Updating RecordManager::DisplayAllOpenFiles}
        This function won't cause any exceptions. Therefore,
        you should add the function specifier, \texttt{noexcept},
        to the end of the signature - both in the declaration and
        the definition.

        \subsubsection*{Updating RecordManager::FindAvailableFile}
        Add the \texttt{noexcept} function specifier.

        \subsubsection*{Updating RecordManager::FindFilenameIndex}
        Add the \texttt{noexcept} function specifier.

        \hrulefill
        \subsubsection*{Testing it out}

        Now when you run the program, it won't flat out crash like
        before, but it will abort the program once the first exception
        is hit.

\begin{framed}
\begin{lstlisting}[style=output]
-------------------------------------
TEST 1: Open one file and write to it

Open files: 
0. Test1.txt

END OF TEST 1

-------------------------------------
TEST 2: Open 5 files and write to them

Open files: 
0. Test2_A.txt
1. Test2_B.txt
2. Test2_C.txt
3. Test2_D.txt
4. Test2_E.txt

END OF TEST 2

-------------------------------------
TEST 3: Write to a file that isn't opened

Open files: 
terminate called after throwing an instance of 'std::runtime_error'
  what():  File Test2.txt is not open
Aborted
\end{lstlisting}
\footnotesize (Note: This is the example output for the program running in Linux,
with the g++ compiler. The result text might look different in Visual Studio.)
\end{framed}

        Next, \texttt{try/catch} blocks will need to be added
        in our tests so that the program will complete its execution,
        even if exceptions are discovered.

        \subsection{Adding try/catch}

            When we are calling a function that may cause an exception,
            that's when we should have a \texttt{try/catch} statement.
            The functions from \texttt{RecordManager} that may cause
            exceptions are:

            \begin{itemize}
                \item \texttt{OpenOutputFile}
                \item \texttt{CloseOutputFile}
                \item \texttt{WriteToFile}
            \end{itemize}

            Test1 and Test2 themselves won't cause any exceptions to be
            thrown, but if we were to add the try/catch to Test1, it would
            look like this:
            
\begin{lstlisting}[style=code]
// (...)
try
{
    record.DisplayAllOpenFiles();
    
    record.WriteToFile( "Test1.txt", "Hello world!" );
    
    record.CloseOutputFile( "Test1.txt" );
}
catch( runtime_error& ex )
{
    cout << "Error: " << ex.what() << endl;
}
// (...)
\end{lstlisting}

            ~\\
            We could wrap each individual function in a try/catch,
            or wrap all three of them together. This is a design decision
            to make. In general, it is considered good design to wrap your
            try/catch around the smallest possible amount of code,
            but it also doesn't need to wrap just one line at a time.
            Since these three functions are related, we can wrap the three
            in a single try/catch block and it would be fairly clean.

            Mainly, don't wrap an entire function in a try/catch -
            only a section working with ``risky" areas.

            \subsection*{Updating Test3, Test4, and Test5}
            For each of these tests, surround only the function calls
            that may return with an exception. Remember that any
            functions marked as \texttt{noexcept} will never throw
            an exception.

            Once you've updated the entire program, the output should
            look like this:

\begin{lstlisting}[style=output]
-------------------------------------
TEST 1: Open one file and write to it

Open files: 
0. Test1.txt

END OF TEST 1

-------------------------------------
TEST 2: Open 5 files and write to them

Open files: 
0. Test2_A.txt
1. Test2_B.txt
2. Test2_C.txt
3. Test2_D.txt
4. Test2_E.txt

END OF TEST 2

-------------------------------------
TEST 3: Write to a file that isn't opened

Open files: 
Error: File Test2.txt is not open

END OF TEST 3

-------------------------------------
TEST 4: Close a file that isn't opened

Open files: 
Error: File Test3.txt is not open

END OF TEST 4

-------------------------------------
TEST 5: Try to open more than max # of files

Error: No available files
Open files: 
0. Test5_A.txt
1. Test5_B.txt
2. Test5_C.txt
3. Test5_D.txt
4. Test5_E.txt

END OF TEST 5
\end{lstlisting}





\input{BASE-FOOT}
