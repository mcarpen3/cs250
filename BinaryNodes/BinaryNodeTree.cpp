//  Created by Frank M. Carrano and Timothy M. Henry.
//  Updated and completed by Matt Carpenter
//  Copyright (c) 2017 Pearson Education, Hoboken, New Jersey.

// Listing 16-3.

/** ADT binary tree: Link-based implementation.
 @file BinaryNodeTree.cpp */

#include "BinaryNodeTree.h"

#include <memory>

//------------------------------------------------------------
// Constructor and Destructor Section.
//------------------------------------------------------------
template <class ItemType>
BinaryNodeTree::BinaryNodeTree() : rootPtr(nullptr)
{

}//end default constructor

template <class ItemType>
BinaryNodeTree::BinaryNodeTree(const ItemType& rootItem)
    :rootPtr(std::make_shared<BinaryNode<ItemType>>(rootItem))
{

}// end constructor

template <class ItemType>
BinaryNodeTree::
BinaryNodeTree(const ItemType& rootItem,
               const std::shared_ptr<BinaryNodeTree<ItemType>> leftTreePtr,
               const std::shared_ptr<BinaryNodeTree<ItemType>> rightTreePtr):
    rootPtr(std::make_shared<BinaryNode<ItemType>>(rootItem,
            copyTree(leftTreePtr->rootPtr),
            copyTree(rightTreePtr->rootPtr)))
{

} // end constructor

template <class ItemType>
BinaryNodeTree::BinaryNodeTree(const std::shared_ptr<BinaryNodeTree<ItemType>>& treePtr)
{
    rootPtr = copyTree(treePtr.rootPtr);
}

template <class ItemType>
virtual BinaryNodeTree::~BinaryNodeTree()
{
    destroyTree(rootPtr);
}

template <class ItemType>
std::shared_ptr<BinaryNode<ItemType>> BinaryNodeTree<ItemType>::copyTree(
                                       const std::shared_ptr<BinaryNode<ItemType>> oldTreeRootPtr) const
{
    std::shared_ptr<BinaryNode<ItemType>> newTreePtr;

//copy tree nodes during a preorder traversal
    if(oldTreeRootPtr !=nullptr)
    {
        newTreePtr = std::make_shared<BinaryNode<ItemType>>(oldTreeRootPtr->getItem(),
                     nullptr,nullptr);
        newTreePtr->setLeftChildPtr(copyTree(oldTreeRootPtr->getLeftChildPtr()));
        newTreePtr->setRightChildPtr(copyTree(oldTreeRootPtr->getRightChildPtr()));
    }
    return newTreePtr;
}//end copyTree




//------------------------------------------------------------
// Protected Utility Methods Section:
// Recursive helper methods for the public methods.
//------------------------------------------------------------
template <class ItemType>
int BinaryNodeTree<ItemType>::getHeightHelper(std::shared_ptr<BinaryNode<ItemType>> subTreePtr) const
{
    if(subTreePtr==nullptr)
        return 0;
    else
        return 1+max(getHeightHelper(subTreePtr->getLeftChildPtr()),(subTreePtr->getRightChildPtr()));
}

template <class ItemType>
int BinaryNodeTree<ItemType>::getNumberOfNodesHelper(std::shared_ptr<BinaryNode<ItemType>> subTreePtr) const
{
    if(subTreePtr==nullptr)
        return 0;
    else
        return 1 + getNumberOfNodesHelper(subTreePtr->getLeftChildPtr())
               + getNumberOfNodesHelper(subTreePtr->getRightChildPtr());
}

// Recursively adds a new node to the tree in a left/right fashion to keep tree balanced.
template <class ItemType>
auto BinaryNodeTree<ItemType>::balancedAdd(std::shared_ptr<BinaryNode<ItemType>> subTreePtr,
        std::shared_ptr<BinaryNode<ItemType>> newNodePtr)
{
    if(subTreePtr == nullptr)
        return newNodePtr;
    else
    {
        auto leftPtr = subTreePtr->getLeftChildPtr();
        auto rightPtr = subTreePtr->getRightChildPtr();

        if(getHeightHelper(leftPtr) > getHeightHelper(right))
        {
            rightPtr = balancedAdd(rightPtr,newNodePtr);
            subTreePtr->setRightChildPtr(rightPtr);
        }
        else
        {
            leftPtr = balancedAdd(leftPtr,newNodePtr);
            subTreePtr->setLeftChildPtr(leftPtr);
        }//endif
        return subTreePtr;
    }//end if
}// end balanced add

// Removes the target value from the tree.
template <class ItemType>
virtual auto BinaryNodeTree<ItemType>::removeValue(std::shared_ptr<BinaryNode<ItemType>> subTreePtr,
        const ItemType target, bool& isSuccessful)
{
}

// Copies values up the tree to overwrite value in current node until
// a leaf is reached; the leaf is then removed, since its value is stored in the parent.
template <class ItemType>
auto BinaryNodeTree<ItemType>::moveValuesUpTree(std::shared_ptr<BinaryNode<ItemType>> subTreePtr)
{
}

// Recursively searches for target value.
template <class ItemType>
virtual auto BinaryNodeTree<ItemType>::findNode(std::shared_ptr<BinaryNode<ItemType>>
        treePtr, const ItemType& target, bool& isSuccessful) const
{
}


// Recursively deletes all nodes from the tree.
template <class ItemType>
void BinaryNodeTree<ItemType>::destroyTree(std::shared_ptr<BinaryNode<ItemType>> subTreePtr)
{
    if(subTreePtr !=nullptr)
    {
        destroyTree(subTreePtr->getLeftChildPtr());
        destroyTree(subTreePtr->getRightChildPtr());
        subTreePtr.reset();//decrement reference count to node
    }//endif
}//end destroyTree

// Recursive traversal helper methods:
template <class ItemType>
void BinaryNodeTree<ItemType>::preorder(void visit(ItemType&), std::shared_ptr<BinaryNode<ItemType>> treePtr) const
{
}

template <class ItemType>
void BinaryNodeTree<ItemType>::inorder(void visit(ItemType&), std::shared_ptr<BinaryNode<ItemType>> treePtr) const
{
}

template <class ItemType>
void BinaryNodeTree<ItemType>::postorder(void visit(ItemType&), std::shared_ptr<BinaryNode<ItemType>> treePtr) const
{
}




//------------------------------------------------------------
// Public BinaryTreeInterface Methods Section.
//------------------------------------------------------------
template <class ItemType>
bool BinaryNodeTree<ItemType>::isEmpty() const
{
    return rootPtr == nullptr;
}

template <class ItemType>
int BinaryNodeTree<ItemType>::getHeight() const
{
    return getHeightHelper(rootPtr);
}

template <class ItemType>
int BinaryNodeTree<ItemType>::getNumberOfNodes() const
{
    return getNumberOfNodesHelper(rootPtr);
}

template <class ItemType>
ItemType getRootData() const throw(PrecondViolatedExcept)
{
}

template <class ItemType>
void BinaryNodeTree<ItemType>::setRootData(const ItemType& newData)
{
    rootPtr->item=newData;
}

template <class ItemType>
bool BinaryNodeTree<ItemType>::add(const ItemType& newData)
{
    auto newNodePtr = std::make_shared<BinaryNode<ItemType>>(newData)
                      rootPtr = balancedAdd(rootPtr,newNodePtr);
    return true;
}
template <class ItemType> // Adds an item to the tree
bool BinaryNodeTree<ItemType>::remove(const ItemType& data)
{}

template <class ItemType> // Removes specified item from the tree
void BinaryNodeTree<ItemType>::clear()
{}

template <class ItemType>
ItemType BinaryNodeTree<ItemType>::getEntry(const ItemType& anEntry) const throw(NotFoundException)
{}

template <class ItemType>
bool BinaryNodeTree<ItemType>::contains(const ItemType& anEntry) const
{}

//------------------------------------------------------------
// Public Traversals Section.
//------------------------------------------------------------
void BinaryNodeTree<ItemType>::preorderTraverse(void visit(ItemType&)) const
{
}

void BinaryNodeTree<ItemType>::inorderTraverse(void visit(ItemType&)) const
{}

void BinaryNodeTree<ItemType>::postorderTraverse(void visit(ItemType&)) const;
{}

//------------------------------------------------------------
// Overloaded Operator Section.
//------------------------------------------------------------
BinaryNodeTree& BinaryNodeTree<ItemType>::operator=(const BinaryNodeTree& rightHandSide)
{
}

}; // end BinaryNodeTree

#include "BinaryNodeTree.cpp"
#endif

