//  Created by Frank M. Carrano and Timothy M. Henry.
//  Copyright (c) 2017 Pearson Education, Hoboken, New Jersey.
#ifndef NOT_FOUND_EXCEPTION
#define NOT_FOUND_EXCEPTION
#include <stdexcept>
#include <string>

class TargetNotFoundException: public std::runtime_error
{
   public :
   TargetNotFoundException(const std::string& message = "")
                         : std::runtime_error("Target not found: " + message)
   {
   } // end constructor
}; // end TargetNotFoundException
#endif
