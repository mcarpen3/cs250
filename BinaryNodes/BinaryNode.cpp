#include "BinaryNode.h"

BinaryNode::BinaryNode():leftChildPtr(nullptr),rightChildPtr(nullptr),item(NULL)
{};
BinaryNode::BinaryNode(const ItemType& anItem):leftChildPtr(nullptr),rightChildPtr(nullptr),item(anItem)
{};
BinaryNode::BinaryNode(const ItemType& anItem,
                       std::shared_ptr<BinaryNode<ItemType>> leftPtr,
                       std::shared_ptr<BinaryNode<ItemType>> rightPtr):item(anItem),
    leftChildPtr(leftPtr),rightChildPtr(rightPtr) {};

void setItem(const ItemType& anItem)
{
    item(anItem);
}
ItemType getItem() const
{
    return item;
}

bool isLeaf() const
{
    return (leftChildPtr == nullptr && rightChildPtr == nullptr);
}

auto getLeftChildPtr() const
{
    return leftChildPtr;
}
auto getRightChildPtr() const
{
    return rightChildPtr;
}

void setLeftChildPtr(std::shared_ptr<BinaryNode<ItemType>> leftPtr)
{
    leftChildPtr = leftPtr;
}
void setRightChildPtr(std::shared_ptr<BinaryNode<ItemType>> rightPtr)
{
    rightChildPtr = rightPtr;
}
