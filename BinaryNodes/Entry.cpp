#include "/home/matt/Documents/DataStructures/Entry.h"

template <class KeyType, class ValueType>
Entry<KeyType,ValueType>::Entry()  //default constructor, c++ will handle default initialization of template "built-in" classes (int, char, string)
    //and other class types will need to have a default constructor to be initialized here, or else an error will occur
{

}

template <class KeyType, class ValueType>
Entry<KeyType,ValueType>::Entry(const KeyType& searchKey, const ValueType& newValue) : key(searchKey), value(newValue)
{
}

template <class KeyType, class ValueType>
ValueType Entry<KeyType,ValueType>::getValue() const
{
    return value;
}

template <class KeyType, class ValueType>
KeyType Entry<KeyType,ValueType>::getKey() const
{
    return key;
}

template <class KeyType, class ValueType>
void Entry<KeyType,ValueType>::setValue(const ValueType& newValue)
{
    value = newValue;
}

template <class KeyType, class ValueType>
void Entry<KeyType,ValueType>::setKey(const KeyType& searchKey)
{
    key = searchKey;
}

template <class KeyType, class ValueType>
bool Entry<KeyType,ValueType>::operator==(const Entry<KeyType, ValueType>& rightHandValue) const
{
    if(this == rightHandValue)
        return true;
    return this->value == rightHandValue->value;
}

template <class KeyType, class ValueType>
bool Entry<KeyType,ValueType>::operator>(const Entry<KeyType, ValueType>& rightHandValue) const
{
    if(this==rightHandValue)
        return false;
    return this->value>rightHandValue->value;
}
