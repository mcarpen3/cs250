#include "/home/matt/Documents/DataStructures/ArrayDictionary.h"


template<class KeyType, class ValueType>
bool ArrayDictionary<KeyType,ValueType>::add(const KeyType& searchKey,
        const ValueType& newValue)throw(PrecondViolatedExcept)
{
    bool ableToInsert = (entryCount < maxEntries);
    if(ableToInsert)
    {

        int entryIndex = entryCount;

        while((entryIndex>0)&&(searchKey<entries[entryIndex-1].getKey()))
        {
            entries[entryIndex] = entries[entryIndex-1];
            entryIndex--;
        }
        if(searchKey!=entries[entryIndex-1].getKey())
        {
            entries[entryIndex] = Entry<KeyType,ValueType>(searchKey,newValue);
            entryCount++;
        }
        else
        {
            auto message = "Attempt to add an entry whose search key exists in dictionary.";
            throw(PrecondViolatedExcept(message));
        }
    }
    return ableToInsert;
}

template<class KeyType, class ValueType>
void ArrayDictionary<KeyType,ValueType>::destroyDictionary()
{}

template<class KeyType, class ValueType>
int ArrayDictionary<KeyType,ValueType>::findEntryIndex(int firstIndex, int lastIndex, const KeyType& searchKey) const
{
    return 0;
}

template<class KeyType, class ValueType>
ArrayDictionary<KeyType,ValueType>::ArrayDictionary()
{}

template<class KeyType, class ValueType>
ArrayDictionary<KeyType,ValueType>::ArrayDictionary(int maxNumberOfEntries)
{}

template<class KeyType, class ValueType>
ArrayDictionary<KeyType,ValueType>::ArrayDictionary(const ArrayDictionary<KeyType, ValueType>& dictionary)
{}

template<class KeyType, class ValueType>
ArrayDictionary<KeyType,ValueType>::~ArrayDictionary()
{}

template<class KeyType, class ValueType>
bool ArrayDictionary<KeyType,ValueType>::isEmpty() const
{
    return false;
}

template<class KeyType, class ValueType>
int ArrayDictionary<KeyType,ValueType>::getNumberOfEntries() const
{
    return 0;
}

template<class KeyType, class ValueType>
bool ArrayDictionary<KeyType,ValueType>::remove(const KeyType& searchKey)
{
    return false;
}

template<class KeyType, class ValueType>
void ArrayDictionary<KeyType,ValueType>::clear()
{}

template<class KeyType, class ValueType>
ValueType ArrayDictionary<KeyType,ValueType>::getValue(const KeyType& searchKey) const throw(TargetNotFoundException)
{
    return searchKey;
}

template<class KeyType, class ValueType>
bool ArrayDictionary<KeyType,ValueType>::contains(const KeyType& searchKey) const
{
    return false;
}

/** Traverses the entries in this dictionary in sorted search-key order
 and calls a given client function once for the value in each entry. */
 template<class KeyType, class ValueType>
void ArrayDictionary<KeyType,ValueType>::traverse(void visit(ValueType&)) const
{}
