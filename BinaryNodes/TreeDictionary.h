//  Created by Frank M. Carrano and Timothy M. Henry.
//  Copyright (c) 2017 Pearson Education, Hoboken, New Jersey.

// Listing 18-4.

/** A binary search tree implementation of the ADT dictionary
 that organizes its entries in sorted search-key order.
 Search keys in the dictionary are unique.
 @file TreeDictionary.h */

#ifndef TREE_DICTIONARY_
#define TREE_DICTIONARY_

#include "/home/matt/Documents/DataStructures/SourceCodeForStudents_7.0/Chapter18/DictionaryInterface.h"
#include "/home/matt/Documents/DataStructures/BinarySearchTree.h"
#include "/home/matt/Documents/DataStructures/Entry.h"
#include "/home/matt/Documents/DataStructures/NotFoundException.h"
#include "/home/matt/Documents/DataStructures/PrecondViolatedExcept.h"

template <class KeyType, class ValueType>
class TreeDictionary : public DictionaryInterface<KeyType, ValueType>
{
private:
   // Binary search tree of dictionary entries
   BinarySearchTree<Entry<KeyType, ValueType> > entryTree;

public:
   TreeDictionary();
   TreeDictionary(const TreeDictionary<KeyType, ValueType>& dictionary);
   virtual ~TreeDictionary();

   // The declarations of the public methods appear here and are the
   // same as given in Listing 18-3 for the class ArrayDictionary.

}; // end TreeDictionary
#include "TreeDictionary.cpp"
#endif
