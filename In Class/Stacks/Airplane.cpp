#include "Airplane.hpp"


void Airplane::Board(Traveller* traveller)
{
    m_passengers.Push(traveller);
    traveller->boarded = true;
}

Traveller* Airplane::Disboard()
{
    Traveller* next = m_passengers.Top();
    m_passengers.Pop();
    next->boarded = false;
    return next;
}

bool Airplane::IsEmpty()
{
    return (m_passengers.Size() == 0);
}
