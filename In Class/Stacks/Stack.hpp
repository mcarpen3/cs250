#ifndef _STACK_HPP
#define _STACK_HPP

#include "LinkedList.hpp"

template <typename T>
class Stack : public LinkedList<T>
{
public:
	Stack() : LinkedList<T>()
	{
	}

	void Push(T& newItem)
	{
        LinkedList<T>::PushBack( newItem );
	}

	void Pop()
	{
        LinkedList<T>::PopBack();
	}

	T& Top()
	{
        return LinkedList<T>::GetBack();
	}

private:
	int m_itemCount;
};

#endif
