#include <iomanip>
#include <iostream>
#include <fstream>
#include <list>
#include <string>
using namespace std;

#include "Menu.hpp"
bool pager_menu();
vector<string> LoadBook(const string& filename);
void ReadBook(vector<string> bookText);

int main()
{
	vector<string> books = { "aesop.txt", "fairytales.txt" };

	bool done = false;
	while (!done)
	{
		Menu::Header("LIBRARY");

		cout << "Which book do you want to read?" << endl;
		int choice = Menu::ShowIntMenuWithPrompt(books);

		vector<string> bookText = LoadBook(books[choice - 1]);
		ReadBook(bookText);
	}

	return 0;
}


vector<string> LoadBook(const string& filename)
{
	vector<string> bookText;

	cout << "Loading " << filename << "..." << endl;

	ifstream input(filename);

	if (!input.good())
	{
		cout << "Error opening file" << endl;
	}

	string line;
	while (getline(input, line))
	{
		bookText.push_back(line);
	}

	cout << endl << bookText.size() << " lines loaded" << endl << endl;

	input.close();

	return bookText;
}


void ReadBook(vector<string> bookText)
{
	system("CLS");
	int pageCount = 0;
	int lineCount = 0;
	int pageLineCount = 0;
	int pageLength = 28;
	bool goBack = false;
	bool goBackFailed = false;
	vector<string>::iterator it = bookText.begin();
	for (it; it != bookText.end(); it++)
	{
		if (pageLineCount == 0)
		{
			cout << *it << endl;
			lineCount++;
			pageLineCount++;
		}
		else {
			if (pageLineCount % pageLength != 0 && !goBackFailed)
			{
				cout << *it << endl;
				lineCount++;
				pageLineCount++;

				if (lineCount == pageLength * 2)
					goBack = true;

			}
			else
			{
				
				if (!goBackFailed)
				{
					cout << "Line " << lineCount << " of " << bookText.size() << endl << endl;
				}
				int choice = Menu::ShowIntMenuWithPrompt({ "BACKWARD","FORWARD" }, false);
				switch (choice)
				{
				case 1:
					if (goBack)
					{
						it -= pageLength * 2;
						lineCount -= pageLength * 2;
						if (lineCount < pageLength * 2)
							goBack = false;
					}
					else
						goBackFailed = true;
					break;
				case 2:
					goBackFailed = false;
					pageLineCount = 0;
					break;
				default:
					break;
				}
			}





		}




	}

}
