#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "DATA_STRUCTURES/Stack.hpp" // LinkedStack

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

void CourseCatalog::LoadCourses() // done
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses()
{
    Menu::Header( "VIEW COURSES" );
	cout << setw(5) << left << " # " << setw(10) << "CODE" << "TITLE" << endl
		<< setw(14) << right << "PREREQS" << endl;
	Menu::DrawHorizontalBar(80, '-');
	for (int i = 0; i < m_courses.Size(); i++)
	{
		cout << " " << setw(4) << left << i << setw(10) << m_courses[i].code << m_courses[i].name << endl;
		if (m_courses[i].prereq != "")
			cout << setw(14) << right << m_courses[i].prereq << endl << endl;
		else
			cout << endl;
	}
	Menu::Pause();
		
}

Course CourseCatalog::FindCourse( const string& code )
{
	
	for (int i = 0; i < m_courses.Size(); i++)
	{
		if (m_courses[i].code==code)
			return m_courses[i];
	}
	
    throw CourseNotFound("Course code " + code + " not found.");
}

void CourseCatalog::ViewPrereqs()
{
    Menu::Header( "GET PREREQS" );
	string courseCode = "";
	Course current;
	cout << " Enter class code" << endl << endl
		<< " >> ";
	cin >> courseCode;
	try
	{
		 current = FindCourse(courseCode);
	}
	catch(CourseNotFound cnf){
		cout << "Error! Unable to find course " << courseCode << "!" << endl;
		return;
	}
	LinkedStack<Course> prereqs;
	prereqs.Push(current);
	while (current.prereq != "")
	{
		
		try {
			current = FindCourse(current.prereq);
		}
		catch (CourseNotFound()) {
			break;
		}
		prereqs.Push(current);
	}
	cout << "Classes to take:" << endl;
	int i = 1;
	Course t_prereq;
	while (!prereqs.IsEmpty())
	{
		t_prereq = prereqs.Top();
		cout << setw(5) << i << "." << t_prereq.code << " " << t_prereq.name << endl;
		prereqs.Pop();
		i++;
	}
}

void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
