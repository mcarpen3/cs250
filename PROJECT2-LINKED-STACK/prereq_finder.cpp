// Done - do not edit

#include <iostream>
using namespace std;

#include "UTILITIES/Menu.hpp"

#include "Tester.hpp"
#include "CourseCatalog.hpp"
#include "DATA_STRUCTURES/ChessBoard.h"
void ChooseSquare(ChessBoard<int>& cb);

int main()
{
    ChessBoard<int> cb;
    cb.initBoard();
	while (true)
	{
		cb.displayBoard();
		ChooseSquare(cb);
	}
    
    
    
//    Menu::Header( "Running tests..." );
//
//    Tester tester;
//    tester.Start();
//    tester.Close();
//
//    cout << endl;
//
//    CourseCatalog courseProgram;
//    courseProgram.Run();
//
//    return 0;
}
void ChooseSquare(ChessBoard<int>& cb)
{
	
	bool success = false;
	int row = 0, col = 0;
	do 
	{
		cout << "Enter a row and column where you would like to put the Queen: " << endl;
		cin >> row >> col;
		cout << "Not a valid square" << endl;
	} while (row > ROWS || col > COLUMNS || row < 1 || col < 1);
	success = cb.placeQueen(row, col);
}