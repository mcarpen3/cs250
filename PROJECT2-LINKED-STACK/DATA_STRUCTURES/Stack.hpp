#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"
#include "LinkedList.hpp"
#include "../EXCEPTIONS/CourseNotFoundException.hpp"

template <typename T>
class LinkedStack : public LinkedList<T> //Linked Stack inherits the LinkedList class template
{
    public:
    LinkedStack() : LinkedList<T>() //initialize the class object same as a LinkedList default
    {

    }

    bool Push( const T& newData ) noexcept // uses LinkedList PushFront() funct.
    {
		return LinkedList<T>::PushBack(newData);
    }

    T& Top() //if list is empty, throw CourseNotFound exception
    {
		if (this->IsEmpty())
			throw CourseNotFound("Top Failed on an empty stack");
		return LinkedList<T>::GetBack();
    }

    bool Pop() noexcept // uses LinkedList::PopBack() funct.
    {
		return LinkedList<T>::PopBack();
    }

    

    private:
   
};

#endif
