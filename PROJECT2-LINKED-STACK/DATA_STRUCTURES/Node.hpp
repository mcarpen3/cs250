#ifndef _NODE_HPP
#define _NODE_HPP


template <typename T>
struct Node
{
    Node();
    Node* ptrNext;
    Node* ptrPrev;
	T data, radiusLeft, radiusRight;
	char col[8];
};

template <typename T>
Node<T>::Node()
{

    ptrNext = ptrPrev = nullptr;
}
#endif
