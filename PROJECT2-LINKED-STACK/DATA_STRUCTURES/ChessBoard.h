/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ChessBoard.h
 * Author: matt
 *
 * Created on March 7, 2018, 8:56 PM
 */

#ifndef CHESSBOARD_H
#define CHESSBOARD_H
#include "LinkedList.hpp"
#include <iomanip>
static int ROWS = 8;
static int COLUMNS = 8;
template <typename T>
class ChessBoard : public LinkedList<T> {
public:
    ChessBoard();
    void PushBack(const T& data, char squareChar);
    void initBoard();
	bool placeQueen(int row, int col);
	
    void displayBoard();
private:
    
};

template <class T>
ChessBoard<T>::ChessBoard() : LinkedList<T>() {

}

template <typename T>
void ChessBoard<T>::initBoard() {
    
    for (int i = 0; i < COLUMNS; i++) {
        PushBack(i, 'O');
    }

}

template <typename T>
void ChessBoard<T>::displayBoard() {
	system("CLS");
    Node<T>* t_row = m_ptrFirst;
	cout << setw(3) << "";
	for (int i = 0; i < COLUMNS; i++)
	{
		cout << i + 1 << ".";
	}
	cout << endl << endl;
    for (int i = 0; i < ROWS; i++) {
		cout << i + 1 << ". ";
        for (int j = 0; j < COLUMNS; j++) {
            cout << t_row->col[j] << " ";
        }
		t_row = t_row->ptrNext;
        cout << endl;
    }
}

template <class T>
void ChessBoard<T>::PushBack(const T& newData, char squareChar) {
    // Allocating memory
    Node<T>* newRow = new Node<T>;
    newRow->data = newData;
    for (int i = 0; i < COLUMNS; i++) {
        newRow->col[i] = squareChar;
    }

    // Place the node in the list:
    // List is empty
    if (this->m_ptrLast == nullptr) {
        // Point the first & last ptrs to the new node
        this->m_ptrFirst = newRow;
        this->m_ptrLast = newRow;
    }
        // List has at least one item
    else {
        // Point the last node's ptrNext to the new node
        this->m_ptrLast->ptrNext = newRow;
        newRow->ptrPrev = this->m_ptrLast;
        this->m_ptrLast = newRow;
    }

    // Increment item count:
    this->m_itemCount++;
    
}

/*
@prereq: row and col ints are <= (STATIC INT ROW, STATIC INT COL) respectively
@post: new queen will be placed at location row, col on the chessboard
as long as it is not in another queen's path. if it is in another queen's path
this function will return false*/
template <class T>
bool ChessBoard<T>::placeQueen(int row, int col)
{
	Node<T>* t_row = this->m_ptrFirst;
	for (int i = 0; i < row - 1; i++)
	{
		t_row->col[col - 1] = 'X';
		t_row = t_row->ptrNext;
		displayBoard();
	}
	//paint the row with "X"s
	for (int i = 0; i < COLUMNS; i++)
	{
		t_row->col[i] = 'X';
		displayBoard();
	}
	//paint the upwards diagonal with 'X's
	Node<T>* t_diag = t_row;
	for (int row_iterator = row - 2; row_iterator >= 0; row_iterator--)
	{
		int next = row - 1 - row_iterator;
		int i = col - 1;
		t_diag = t_diag->ptrPrev;
		if (i <= COLUMNS - 1)
			t_diag->col[i + next] = 'X';
		displayBoard();
	}
	return true;
}

#endif /* CHESSBOARD_H */

