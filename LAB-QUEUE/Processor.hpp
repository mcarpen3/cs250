#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"
#include "Menu.hpp"
class RunTimeException : public runtime_error
{
	RunTimeException(string error) : runtime_error(error)
	{

	}
};
class Processor
{
public:
	void FirstComeFirstServe(vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile);
	void RoundRobin(vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile);
};

void Processor::FirstComeFirstServe(vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile)
{
	ofstream output(logFile);
	output << "First Come, First Served" << endl;

	int cycles = 0;

	output << "Currently processing Job #" << jobQueue.Front()->id << endl;
	//While jobQueue is not empty...
	while (jobQueue.Size() > 0)
	{

		jobQueue.Front()->Work(FCFS);
		output << "\tCYCLE\t" << cycles << "\t\tREMAINING:\t" << jobQueue.Front()->fcfs_timeRemaining << endl;
		cycles++;
		if (jobQueue.Front()->fcfs_done)
		{

			jobQueue.Front()->SetFinishTime(cycles, FCFS);
			jobQueue.Pop();
			output << "Done\n\n";
			Menu::DrawHorizontalBar(45, output);
			if (jobQueue.Size() > 0)
				output << "Currently processing Job #" << jobQueue.Front()->id << endl;

		}


	}
	output << endl;
	output << "\nFirst come, first served results:" << endl;
	string header1 = "JOB ID", header2 = "TIME TO COMPLETE";
	int total_time = 0, avg_time = 0;
	int col1 = header1.size() + 4;
	output << setw(col1) << left << header1 << header2 << endl;
	for (auto it = allJobs.begin(); it != allJobs.end(); it++)
	{
		output << setw(col1) << it->id << it->fcfs_finishTime << endl;
		total_time += it->fcfs_finishTime;
	}
	avg_time = total_time / allJobs.size();
	output << endl;
	string total = "Total time:", avg = "Average time:";
	int width = avg.size() + 12;
	output << total;
	for (int i = total.size(); i < width; i++)
	{
		output << ".";
	}
	output << " " << total_time;
	output << "\n" << setw(4) << left << "" << "(Time for all jobs to complete processing)" << endl << endl;
	output << avg;
	for (int i = avg.size(); i < width; i++)
	{
		output << ".";
	}
	output << " " << avg_time << endl;
	output << setw(4) << left << "" << "(The average time to complete, including the wait time" << endl << setw(4) << "" << "while items are ahead of it in the queue.)" << endl;
}

void Processor::RoundRobin(vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile)
{
	ofstream output(logFile);
	int cycles = 0;
	int timer = 0;
	output << "Round Robin" << endl;


	Menu::DrawHorizontalBar(45, output);
	output << "Currently processing Job #" << jobQueue.Front()->id << endl;
	while (jobQueue.Size() > 0)
	{
		output << "\tCYCLE\t" << cycles << "\t\tREMAINING:\t\t" << jobQueue.Front()->rr_timeRemaining << endl;
		jobQueue.Front()->Work(RR);
		cycles++;
		timer++;
		if (jobQueue.Front()->rr_done)
		{
			jobQueue.Front()->SetFinishTime(cycles, RR);
			if (jobQueue.Pop())
			{
				if (jobQueue.Size() > 0)
				{
					output << "Processing job #" << jobQueue.Front()->id << "..." << endl;
					Menu::DrawHorizontalBar(45, output);
					timer = 0;
				}

			}
		}
		if (timer == timePerProcess)
		{
			jobQueue.Front()->rr_timesInterrupted++;
			jobQueue.Push(jobQueue.Front());
			if (jobQueue.Pop())
			{

				output << "Processing job #" << jobQueue.Front()->id << "..." << endl;
				Menu::DrawHorizontalBar(45, output);
				timer = 0;
			}


		}
	}
	output << endl;
	output << "Round Robin results:\n";

	output << endl;
	string header1 = "JOB ID", header2 = "TIME TO COMPLETE", header3 = "TIMES INTERRUPTED";
	int total_time = 0, avg_time = 0, col1 = header1.size() + 4, col2 = header2.size() + 4;

	output << setw(col1) << left << header1 << setw(col2) << header2 << header3 << endl;
	for (auto it = allJobs.begin(); it != allJobs.end(); it++)
	{
		output << setw(col1) << left << it->id << setw(col2) << it->rr_finishTime << it->rr_timesInterrupted << endl;
		total_time += it->rr_finishTime;
	}
	output << endl;
	avg_time = total_time / allJobs.size();
	string total = "Total time:", avg = "Average Time:", rr_int = "Round robin interval:";
	int width = avg.size() + 12;
	output << total;
	for (int i = total.size(); i < width; i++)
	{
		output << ".";
	}
	output << " " << total_time << endl;
	output << setw(4) << left << "" << "(Time for all jobs to complete processing)" << endl << endl;
	output << avg;
	for (int i = avg.size(); i < width; i++)
	{
		output << ".";
	}
	output << " " << avg_time << endl;
	output << setw(4) << left << "" << "(The average time to complete, including the wait time" << endl << setw(4) << "" << "while items are ahead of it in the queue.)" << endl << endl;
	output << rr_int;
	for (int i = rr_int.size(); i < width; i++)
	{
		output << ".";
	}
	output << " " << timePerProcess << endl;
	output << setw(4) << left << "" << "(Every n units of time, move the current item being processed" <<
		endl << setw(4) << "" << "to the back of the queue and start working on the next item)";

}

#endif
