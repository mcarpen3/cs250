#pragma once
#include <stdexcept>
class PrecondViolatedException : public std::exception
{
public:
	PrecondViolatedException(const std::string& message = "") : std::exception("Precondition violated: " + message)
	{

	}//end constructor
};//end PrecondViolatedException