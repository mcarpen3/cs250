#pragma once

template<class ItemType>
class Node
{
private:
	ItemType item; // data item
	Node<ItemType>* next; //Pointer to next node
public:
	Node();
	Node(const ItemType& anItem);
	Node(const ItemType& anItem, Node<ItemType>* nextNodePtr);
	
	/*Sets the value of the node's item to anItem
	@pre  None
	@post   The value of item equals anItem
	@param  anItem  The value to be assigned to item.*/
	void setItem(const ItemType& anItem);

	/*Sets the value of the next pointer to nextNodePtr
	@pre  None
	@post  The value of next is set to nextNodePtr.
	@param  nextNodePtr  the pointer value to assign to next. */
	void setNext(Node<ItemType>* nextNodePtr);

	/*Returns a copy of the value in the item's item member.
	@pre None
	@post  The value stored in item is returned to the caller.*/
	ItemType getItem() const;

	/*Returns the pointer in next, which points to the next node.
	@pre None
	@post  The pointer next that points to the next Node is returned
	to the caller.*/
	Node<ItemType>* getNext() const;
}; //end node
