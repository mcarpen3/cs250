// InstanceCreator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
using namespace carpenter;
int searchInterface(Table &);
std::string choosePart();
std::string dispStandard();
std::string dispTitle(std::string part_name_replacement, std::string Instance_name);
std::string get_serial();
std::string validate_serial_number(std::string part_number,std::string serial);
std::string generate_problem_description(std::string part_name, std::string Instance_name, std::string head, std::string contact);
std::string get_part_name(std::string part_number);
std::string NowToString();
bool yesOrNo(std::string message);
void warranty_case_menu();
void main_menu();
void royal_menu();
void royal_case();
void part_search();
void udf_parse();
void test_parse();
std::string get_contact();
void clipboardMessage(std::string item);
std::string shipping_instructions = "Standard Shipping to the Weston, FL warehouse.";
int main()
{
	system("CLS");
	main_menu();

    return 0;
}
void main_menu()
{
	system("cls");
	int choice;

	while (true)
	{
		std::cout
			<< "\n1. Create a warranty case." << std::endl
			<< "\n2. Royal Caribbean Customer Operations." << std::endl

			<< "\n3. Search for Digital Ally part numbers." << std::endl
			<< "\n4. Parse ship UDF file." << std::endl
			<< "\nChoose an operation: ";
		std::cin >> choice;
		switch (choice)
		{
		case 1:
			warranty_case_menu();
		case 2:
			royal_menu();
			break;
		case 3:
			part_search();
			break;
		case 4:
			udf_parse();
			break;
		case 5:
			test_parse();
			break;
		default:
			std::cout << "Please choose a number 1 through 2" << std::endl;
		}
	}
}
void royal_menu()
{
	system("CLS");
	Table s;
std::vector<int> field_widths = s.parseTableFile("rcShips.csv");
	
	int inst; 
	int choice;
	std::string pn = "";
	std::string pname = "";
	while (true)
	{
		system("CLS");
		std::cout << "\n1. Create a new Royal Caribbean case." << std::endl
			<< "2. Get Shipping Instructions." << std::endl
			<< "3. Search for ship." << std::endl
			<< "4. Get part information." << std::endl
			<< "5. Display all ships." << std::endl
			<< "6. Get MyName and Date." << std::endl
			<< "7. Back to main menu." << std::endl
			<< "Enter a choice: ";
		std::cin >> choice;
		switch (choice)
		{
		case 1:
			royal_case();
			break;
		case 2:
			clipboardMessage("Shipping instructions");
			toClipboard(shipping_instructions);
			break;
		case 3:
			
			inst = searchInterface(s);
			
			toClipboard(s.display_instance(inst));
			clipboardMessage("Ship info");
			break;
		case 4:
			pn = choosePart();
			toClipboard(pn);
			clipboardMessage("Part number");
			break;
		case 5:
			s.display(field_widths);
			system("PAUSE");
			break;
		case 6:
			toClipboard("--Matt Carpenter--\n" + NowToString() + "--------------------------------");
			clipboardMessage("MyName and Date");
			break;
		case 7:
			main_menu();
			break;
		default:
			std::cout << "Please choose a number 1 through 3" << std::endl;
		}
	}
}
void royal_case()
{
	system("CLS");
	Table s;
	
	std::string pn = "";
	std::string prob_desc_header = "";
	std::string Instance_info = "";
	std::string pname = "";
	std::string Instance_name = "";
	std::string title = "";
	std::string name = "";
	std::string case_serial = "";
	std::string prob_desc = "";
	int item_index;
	std::string my_name = " Matt Carpenter\n";
	std::string pri_contact = "Jensen Sostre";
	std::string contact = "";
	std::string validated_serial = "";
	
	bool done = false;

	contact = get_contact();
	if (contact == "quit")
	{
		royal_menu();
	}
	s.parseTableFile("rcShips.csv");
	char choice;
	while (done == false)
	{
		item_index = searchInterface(s);
		if (item_index == -1)
		{
			royal_menu();
		}
		done = yesOrNo("is this the correct information? ");
		
		
	}
	Instance_name = s.get_name(item_index);
	
	bool doneWithShip = false;
	while (!doneWithShip)
	{
		bool valid_serial = false;
		pn = choosePart();
		pname = get_part_name(pn);
		prob_desc_header = NowToString();
		prob_desc_header += my_name;
		while (!valid_serial)
		{
			case_serial = get_serial();
			validated_serial = validate_serial_number(pn, case_serial);
			if (validated_serial.length() > 1)
				valid_serial = true;
		}
		prob_desc = generate_problem_description(pname, Instance_name, prob_desc_header, contact);
		title = dispTitle(pn, Instance_name) + "\n";
		std::cout << prob_desc_header;
		system("CLS");
		std::cout << "\nInformation saved!" << std::endl;
		Instance_info = s.display_instance(item_index);
		std::cout << "Case title created: " << title;
		toClipboard(title);
		clipboardMessage("Case title");

		toClipboard(pri_contact);
		clipboardMessage("Primary contact");

		toClipboard(pn);
		clipboardMessage("Part number");

		toClipboard(case_serial);
		clipboardMessage("Serial number");

		toClipboard(prob_desc);
		clipboardMessage("Problem description");

		toClipboard("Standard Shipping to the Weston, FL warehouse.");
		
		clipboardMessage("Shipping Instructions");

		toClipboard(Instance_info);
		clipboardMessage("Ship initials and purchase order number");


		
		doneWithShip = !yesOrNo("Do you have another case for this ship? ");
	}
	}
	
	
		
		
		
		
		
		
		
		
		
		
		
	
	
	
	
std::string NowToString()
{
	std::chrono::system_clock::time_point p = std::chrono::system_clock::now();
	time_t t = std::chrono::system_clock::to_time_t(p);
	char str[26];
	ctime_s(str, sizeof str, &t);
	return str;
}
int searchInterface(Table & Table_ref_par)
{
	std::string initials;
	while (true) {
		std::cout << "\nSearch for Instance by initials, type exit to quit: ";
		std::cin >> initials;
		if (initials == "exit")
		{
			return -1;
		}
		for (int i = 0; initials[i] != '\0'; i++)
		{
			initials[i] = toupper(initials[i]);
		}
		int temp = Table_ref_par.search(initials);
		if (temp != -1)
		{
			std::cout << "\nItem Located." << std::endl;

			return temp;
		}
		else{ 
			std::cout << "\nItem not Found." << std::endl; 
			
		}
			

	}


}
std::string dispStandard()
{
	return "Standard Shipping to the Weston, FL warehouse.";

}
std::string choosePart()
{
	int part;
	
	while(true)
	{
		std::cout
			<< "\n1. 11\" camera head" << std::endl
			<< "2. 48\" camera head" << std::endl
			<< "3. DVR" << std::endl
			<< "4. Extended Battery" << std::endl
			<< "5. Back to main menu." << std::endl
			<< "Select an option: ";
		std::cin >> part;
		switch (part)
		{
		case 1:
			return "R001-00275-11";
			break;
		case 2:
			return "R001-00275-48";
			break;
		case 3:
			return "001-00280-00";
			break;
		case 4:
			return "135-00407-00";
		case 5:
			main_menu();
			break;
		default:
			std::cout << "Please choose from options 1 through 4" << std::endl;
		}
	}
	
}
std::string get_part_name(std::string part_number)
{
	
	std::string part_name = "";
	
	if (part_number == "R001-00275-11")
		part_name = "11\" camera head";
	else if (part_number == "R001-00275-48")
		part_name = "48\" camera head";
	else if (part_number == "001-00280-00")
		part_name = "DVR";
	else if (part_number == "135-00407-00")
		part_name = "Extended Life Battery";
		
	return part_name;


}
std::string dispTitle(std::string part_number, std::string Instance_name)
{
	std::string part_name = "";
	if (part_number == "R001-00275-11")
		part_name = "11\" camera head replacement -- ";
	else if (part_number == "R001-00275-48")
		part_name = "48\" camera head replacement -- ";
	else if (part_number == "001-00280-00")
		part_name = "DVR replacement -- ";
	else
		part_name = "";
	return part_name + Instance_name;

	
}
std::string validate_serial_number(std::string part_number, std::string serial)
{
	int convert_worker = 0;
	
	if (part_number == "R001-00275-11" || part_number == "R001-00275-48")
		convert_worker = 0;
	else if (part_number == "001-00280-00")
		convert_worker = 1;
	else if (part_number == "135-00407-00")
		convert_worker = 2;
	else
	{
		std::cout << "error processing the request, exiting. ";
		system("pause");
		exit(1);
	}
	switch (convert_worker)
	{
	case 0 :
		if (serial.length() != 12)
		{
			std::cout << "\nInvalid serial number.\n";
			return "";
		}
		return serial;
		break;
	case 1:
		if(serial.length()!=8)
		{
			std::cout << "\nInvalid serial number.\n";
			return "";
		}
		return serial;
		break;
	case 2:
		if (serial.length() >= 0)
			return serial;
		break;
	default:
		std::cout << "error processing request, exiting. ";
		system("Pause");
		exit(1);

	}
	
}
std::string generate_problem_description(std::string part_name, std::string Instance_name, std::string head, std::string contact)
{
	int problem;
	int trouble;
	std::string complete_desc = "";
	std::string custom_desc_string = "";
	std::string custom_trouble_string = "";
	
	std::string str = "";
	std::vector<std::string> prob_desc = { "recording blurry videos.",
											"recording videos with vertical lines of color distortion.",
											"triggering random recordings.",
											"causing error mode.",
											"causing random record triggers and error mode.",
											"having a damaged connector.",
											"failing to activate the FirstVu HD."
	};
	std::vector<std::string> trouble_desc = {
		"swapped this camera head with a known working one to resolve the issue.",
		"swapped this camera head to a different DVR and the issue followed."
	};
	
	
	

	while (true)
	{
		std::cout << "\nPlease select the appropriate problem description: " << std::endl;
		std::cout << "1. Blurry video.\n";
		std::cout << "2. Distorted video (vertical lines).\n";
		std::cout << "3. Random recording triggers.\n";
		std::cout << "4. Error mode.\n";
		std::cout << "5. Random recording triggers and error mode.\n";
		std::cout << "6. Damaged connector.\n";
		std::cout << "7. No record trigger.\n";
		std::cout << "8. None of the above.\n> ";
		std::cin >> problem;
		std::cin.ignore();
		if (problem > 0 && problem < 9)
		{

			break;
		}
		else
		{
			std::cout << " Invalid option." << std::endl;
		}

	}
	if (problem == 8)
	{
		std::vector<char> custom_desc;
		char next;
		std::cout << "\nPlease enter the custom description.\n> ";
		std::cin.get(next);
		while (next != '\n')
		{
			custom_desc.push_back(next);
			std::cin.get(next);
		}
		if (custom_desc.empty() == false)
			custom_desc_string = std::string(custom_desc.begin(), custom_desc.end());
		prob_desc.push_back(custom_desc_string);
	}
	while (true)
	{
		std::cout << "\nPlease choose an appropriate troubleshooting description: " << std::endl;
		std::cout << "1. Swapped the camera head with known-working camera head to resolve the issue.\n" <<
			"2. Tested the malfunctioning camera head on a different DVR and the issue followed.\n" <<
			"3. None of the above.\n> ";
		std::cin >> trouble;
		std::cin.ignore();
		if (trouble > 0 && trouble < 4)
			break;
		else {
			std::cout << " Invalid trouble description selection. " << std::endl;
		}
	}
	if (trouble == 3)
	{
		std::vector<char> custom_trouble;
		char next;
		std::cout << "\nPlease enter a custom troubleshooting description.\n> ";
		std::cin.get(next);
		
		while(next!='\n'){
			custom_trouble.push_back(next);
			std::cin.get(next);
		}
		if (custom_trouble.empty() == false)
			custom_trouble_string = std::string(custom_trouble.begin(), custom_trouble.end());
		trouble_desc.push_back(custom_trouble_string);
		
	}
	str = contact + " from " + Instance_name;
	complete_desc = head + str + " submitted a service request for this " + part_name + " because it was reported " + prob_desc[problem-1] + "\n"
		+ "Customer " + trouble_desc[trouble-1] + "\nCustomer is requesting RMA for " + part_name;
	return complete_desc;

}
void part_search()
{
	std::cout << "Enter a part number: ";
	Table p;
	std::vector<int> field_widths = p.parseTableFile("simple_part_list.csv");
	p.display(field_widths);

}
void clipboardMessage(std::string item)
{
	std::cout << item << " copied to clipboard, use CTRL-V to copy information into CRM." << std::endl;
	system("pause");
}
std::string get_contact()
{
	std::cin.ignore();
	std::vector<char> primary_contact;
	std::string str = "";
	std::cout << "\nPlease enter the name of the contact, or type quit to exit: ";
	char next;
	int i = 0;
	std::cin.get(next);
	while (next != '\n')
	{

		primary_contact.push_back(next);
		i++;
		std::cin.get(next);

	}
	if (primary_contact.empty() == false)
	{
		str = std::string(primary_contact.begin(), primary_contact.end());
	}
	return str;
}
std::string get_serial()
{
	std::string serial_number;
	std::string prompt = "\nEnter the serial number with no spaces or dashes: ";
	std::cout << prompt;
	std::cin >> serial_number;
	return serial_number;
}
bool yesOrNo(std::string message)
{
	char choice;
	std::cout << message << "(y or n): ";
	std::cin >> choice;
	while (tolower(choice) != 'y' && tolower(choice) != 'n')
	{
		std::cout << "\nplease type \'y\' for yes or \'n\' for no." << std::endl
			<< message << "(y or n): ";
		std::cin >> choice;
	}
	return choice == 'y';
}
//std::string get_custom_part_number()
//{
//	char next;
//	std::vector<char> part_name;
//	std::cout << "\nPlease enter the part name: ";
//	std::cin.get(next);
//	while (next != '\n')
//	{
//		part_name.push_back(next);
//
//	}
//	Table c;
//	c.parseTableFile("simple_part_list.csv");
//}
void warranty_case_menu()
{
	Table p;
	std::vector<int> field_widths = p.parseTableFile("simple_part_list.csv");
	p.display(field_widths);
	system("pause");
}
void udf_parse()
{
	Table u;
	std::vector<int> field_widths = u.parseTableFile("INudf.csv");
	u.display(field_widths);
	u.display_column(0);
	system("pause");
}
void test_parse()
{
	Table t;
	std::vector<int> field_widths = t.parseTableFile("test_mini_part_list");
	t.display(field_widths);
	system("pause");
}