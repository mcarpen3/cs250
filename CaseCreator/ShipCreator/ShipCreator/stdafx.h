// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Windows.h>
#include <iomanip>
#include <ctime>
#include <chrono>


namespace carpenter
{

	struct Instance
	{
		std::vector<std::string> attributes;
		Instance* link;
	};
	typedef Instance* InstPtr;
	class Table
	{
	public:
		Table();
		Table(const Table& right);
		~Table();
		std::vector<int> parseTableFile(std::string filename);
		void push(std::vector<std::string> args);
		void pop();
		bool isEmpty() const;
		int search(std::string initials);
		int getAttribCount(int InstanceIndex);
		void display(std::vector<int> field_widths) const;
		int getInstanceCount() const;
		void display_column(int column) const;
		std::string getAttribute(int index, int instance);
		std::string display_instance(int index);
		std::string get_name(int index);
		
		

	private:
		int size;
		InstPtr head, tail;
	};
	void toClipboard(const std::string &s);

}


// TODO: reference additional headers your program requires here
