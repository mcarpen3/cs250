// stdafx.cpp : source file that includes just the standard includes
// $safeprojectname$.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file



using namespace carpenter;

Table::Table() : head(nullptr), tail(nullptr), size(0)
{

}
Table::Table(const Table& right)
{
	using std::vector;
	using std::string;
	vector<string> temp_attrib;
	if (right.isEmpty())
	{
		this->tail = nullptr;
		this->head = nullptr;
		size = 0;
	}
	else
	{
		InstPtr work = right.head;
		InstPtr work1 = this->head;
		work1 = new Instance();

		for (int i = 0; i < work->attributes.size(); i++)
		{
			work1->attributes.push_back(work->attributes[i]);
		}
		size++;
		work1->link = nullptr;
		while (work->link != nullptr)
		{
			work1->link = new Instance();
			work1 = work1->link;
			work = work->link;
			for (int i = 0; i < work->attributes.size(); i++)
			{
				work1->attributes.push_back(work->attributes[i]);
			}
		}
		work1->link = nullptr;
		this->tail = work1;
	}
}
Table::~Table()
{
	while (!isEmpty())
	{
		pop();
	}
}
void Table::push(std::vector<std::string> args)
{

	if (isEmpty())
	{
		head = new Instance();

		head->attributes = args;
		tail = head;
		size++;
	}
	else
	{
		tail->link = new Instance();
		tail = tail->link;
		tail->attributes = args;

		tail->link = nullptr;
		size++;
	}


}
void Table::pop()
{
	if (isEmpty())
	{
		std::cout << "error popping an empty stack";
		exit(1);
	}


	InstPtr temp = head;
	head = head->link;
	delete temp;




}
void Table::display(std::vector<int> field_widths) const
{

	if (isEmpty())
	{
		std::cout << "nothing to display";
	}

	else
	{
		InstPtr temp = head;
		int i = 0;
		int temp_width;
		while (temp != nullptr)
		{
			for (auto it = temp->attributes.begin(); it != temp->attributes.end(); it++)
			{
				temp_width = field_widths[i];
				std::cout << std::left << std::setw(temp_width) << *it;
				i++;
			}
			i = 0;
			std::cout << std::endl;
			temp = temp->link;
		}
	}


}

std::string Table::display_instance(int index)
{
	if (isEmpty())
		return "Nothing to display";
	InstPtr temp = head;
	int count = 0;
	std::string work = "";
	while (count != index)
	{
		temp = temp->link;
		count++;
	}
	std::cout << std::endl;
	work = temp->attributes[0] + "\n" + temp->attributes[2];
	return work;

}


bool Table::isEmpty() const
{
	return head == nullptr;
}
int Table::search(std::string search_string)
{
	if (isEmpty())
	{
		std::cout << "Stack is empty.";
		return -1;
	}

	int index = 0;
	InstPtr search = head;
	while (search->attributes[0] != search_string)
	{
		if (search->link != nullptr)
		{
			search = search->link;
			index++;
		}
		else
		{
			return -1;
		}

	}

	std::cout << search->attributes[0] << std::endl << search->attributes[1] << std::endl << search->attributes[2] << std::endl;

	return index;



}

std::vector<int> Table::parseTableFile(std::string filename)
{
	std::vector<int> attributeMaxSize;
	int attributeCount = 0;
	std::ifstream in;
	std::string temp_attribute = "";
	std::string temp_prevAttribute = "";
	in.open(filename);
	int inst_var_iter = 0;
	if (in.fail())

	{
		std::cout << "Error opening file " << filename << " exiting program. ";
		system("PAUSE");
		exit(1);
	}

	char next;
	in.get(next);
	std::vector<char> CellVars;
	std::vector<std::string> InstanceVars;
	bool quotedString = false;
	bool american_std = false;


	while (!(in.eof()))
	{

		while (next != '\n' && !in.eof())
		{

			while (quotedString)
			{

				CellVars.push_back(next);
				in.get(next);
				while (next != '\"')
				{
					CellVars.push_back(next);
					in.get(next);
				}
				if (next == '\"')
				{
					CellVars.push_back(next);
					in.get(next);
					if (next == '\"')
						in.get(next);
					else
						quotedString = false;
				}





			}
			while (next != ',' && next != '\n' && !in.eof() && next != '\"' && !quotedString)

			{


				CellVars.push_back(next);
				in.get(next);




			}
			if (next != '\n' && next != '\"' && next == ',')
			{

				while (next == ',' && !in.eof())
					in.get(next);
				if (next == '\"')
					quotedString = true;
			}

			temp_attribute = std::string(CellVars.begin(), CellVars.end());
			InstanceVars.push_back(temp_attribute);
			
			
			if (isEmpty() || InstanceVars.size() > attributeMaxSize.size())
			{
				attributeMaxSize.push_back(temp_attribute.size() + 2);
			}
			else if (temp_attribute.size() > attributeMaxSize[inst_var_iter] - 2)
			{
				attributeMaxSize[inst_var_iter] = temp_attribute.size() + 2;
			}

			inst_var_iter++;

			temp_attribute = "";
			temp_prevAttribute = "";
			CellVars.clear();


		}

		this->push(InstanceVars);
		InstanceVars.clear();
		in.get(next);
		inst_var_iter = 0;
		/*size++;*/

	}

	in.close();
	return attributeMaxSize;
}

void carpenter::toClipboard(const std::string &s) {
	OpenClipboard(0);
	EmptyClipboard();
	HGLOBAL hg = GlobalAlloc(GMEM_MOVEABLE, s.size() + 1);
	if (!hg) {
		CloseClipboard();
		return;
	}
	memcpy(GlobalLock(hg), s.c_str(), s.size());
	GlobalUnlock(hg);
	SetClipboardData(CF_TEXT, hg);
	CloseClipboard();
	GlobalFree(hg);
}
std::string Table::get_name(int index)
{
	int i = 0;
	InstPtr temp_ptr = head;
	while (i != index)
	{
		temp_ptr = temp_ptr->link;
		i++;
	}
	return temp_ptr->attributes[1];


}
int Table::getInstanceCount() const
{
	return size;
}
int Table::getAttribCount(int InstanceIndex)
{
	if (isEmpty() || getInstanceCount() - 1 < InstanceIndex)
		return -1;
	else
	{
		InstPtr temp_ptr = head;
		for (int i = 0; i < InstanceIndex; i++)
		{

			temp_ptr = temp_ptr->link;
		}
		return temp_ptr->attributes.size();
	}


}
std::string Table::getAttribute(int index, int instance)
{
	if (isEmpty() || index >= getAttribCount(instance) || instance >= getInstanceCount())
		return "";
	InstPtr temp_ptr = head;
	for (int i = 0; i < instance; i++)
	{
		temp_ptr = temp_ptr->link;
	}
	return temp_ptr->attributes[index];
}
//void testTextFile(std::string filename)
//{
//	std::ifstream in;
//	in.open(filename);
//	if (in.fail())
//	{
//		std::cout << "error opening file..exiting program." << std::endl;
//		exit(1);
//	}
//	char next;
//	in.get(next);
//	while (!in.eof())
//	{
//		std::cout << next;
//		in.get(next);
//	}
//	in.close();
//}
void Table::display_column(int column) const
{
	InstPtr temp = head;
	while (temp != nullptr)
	{
		std::cout << "<ListItem>" << temp->attributes[column] << "</ListItem>";
		temp = temp->link;
	}
}