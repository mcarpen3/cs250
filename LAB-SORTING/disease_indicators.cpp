#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <fstream>
#include <map>
using namespace std;

#include "Timer.hpp"
#include "Menu.hpp"

struct DataEntry {
    map<string, string> fields;

    void Output(ofstream& output) {
        for (map<string, string>::iterator it = fields.begin();
                it != fields.end(); it++) {
            output << left << setw(30) << it->second;
        }
        output << endl;
    }

};

class FileNotFoundException : public runtime_error {
public:

    FileNotFoundException(const string& error) : runtime_error(error) {

    }
};



void ReadData(vector<DataEntry>& data, const string& filename);

int SelectionSort(vector<DataEntry>& data, const string& onKey);

int InsertionSort(vector<DataEntry>& data, const string& onKey);

int BubbleSort(vector<DataEntry>& data, const string& onKey);

int QuickSort(vector<DataEntry>& data, const string& onKey);

void QuickSort_Recursive(vector<DataEntry>& data, int low, int high, int& iterations, const string& onKey);

int Partition(vector<DataEntry>& data, int low, int high, int& iterations, const string& onKey);

int MergeSort(vector<DataEntry>& data, const string& onKey);

void MergeSort_Recursive(vector<DataEntry>& data, int first, int last, int& iterations, const string& onKey);

void Merge(vector<DataEntry>& data, int first, int mid, int last, int& iterations, const string& onKey);

void TestOutput(const vector<DataEntry>& data, const string& onKey, int idx_i, int idx_j, int min);

//string GetMax(vector<DataEntry>& data, const string& onKey);

//int RadixSort(vector<DataEntry>& data, const string& onKey);

//void CountSort(vector<DataEntry>& data, int exp, int& iterations);

int main() {
    Menu::Header("U.S. Chronic Disease Indicators");

    vector<string> filenames = {
        "10_US_Chronic_Disease_Indicators.csv",
        "50_US_Chronic_Disease_Indicators.csv",
        "100_US_Chronic_Disease_Indicators.csv",
        "500_US_Chronic_Disease_Indicators.csv",
        "1000_US_Chronic_Disease_Indicators.csv",
        "5000_US_Chronic_Disease_Indicators.csv",
        "10000_US_Chronic_Disease_Indicators.csv",
        "523487_US_Chronic_Disease_Indicators.csv",
        "QUIT"
    };

    //Sorting Algorithms			Efficiency in BigO(n) notation
    vector<string> sorts = {
        "Selection Sort", //	BigO(n^2)
        "Insertion Sort", //	Best: BigO(n) || Worst: BigO(n^2)
        "Bubble Sort", //		Best: BigO(n) || Worst: BigO(n^2)		
        "Quick Sort", //		Best: BigO(n log n) || Worst: BigO(n^2)
        "Merge Sort", //		BigO(n log n)
        "QUIT"
    };

    vector<string> columns = {
        "YearStart", "YearEnd", "LocationAbbr", "LocationDesc", "Topic", "Question"
    };

    while (true) {
        cout << "Which file do you want to load?" << endl;
        int fileChoice = Menu::ShowIntMenuWithPrompt(filenames);

        if (filenames[fileChoice - 1] == "QUIT")
            break;

        cout << "Which sort do you want to use?" << endl;
        int sortChoice = Menu::ShowIntMenuWithPrompt(sorts);

        if (sorts[sortChoice - 1] == "QUIT")
            break;

        cout << "Which column do you want to sort on?" << endl;
        int sortOnChoice = Menu::ShowIntMenuWithPrompt(columns);

        cout << fileChoice << ", " << sortChoice << ", " << sortOnChoice << endl;

        Timer timer;
        vector<DataEntry> data;

        string filename = filenames[fileChoice - 1];


        // Read in the data from the file
        cout << left << setw(15) << "BEGIN:" << "Loading data from file, \"" << filename << "\"..." << endl;
        timer.Start();
        try {
            ReadData(data, filename);
        } catch (FileNotFoundException ex) {
            cout << ex.what();
            exit(1);
        }

        cout << left << setw(15) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds" << endl << endl;

        cout << data.size() << " items loaded" << endl;


        // Sort the data


        if (sortChoice == 1) {
            cout << left << setw(15) << "BEGIN:" << "Sorting data with Selection Sort..." << endl;
            timer.Start();

            int iterations = SelectionSort(data, columns[sortOnChoice - 1]);
            cout << left << setw(15) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds" << " in " << iterations << " iterations." << endl << endl;
        } else if (sortChoice == 2) {
            cout << left << setw(15) << "BEGIN:" << "Sorting data with Insertion Sort..." << endl;
            timer.Start();
            int iterations = InsertionSort(data, columns[sortOnChoice - 1]);
            cout << left << setw(15) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds" << " in " << iterations << " iterations." << endl << endl;
        } else if (sortChoice == 3) {
            cout << left << setw(15) << "BEGIN:" << "Sorting data with Bubble Sort..." << endl;
            timer.Start();
            int iterations = BubbleSort(data, columns[sortOnChoice - 1]);
            cout << left << setw(15) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds" << " in " << iterations << " iterations." << endl << endl;
        } else if (sortChoice == 4) {
            cout << left << setw(15) << "BEGIN:" << "Sorting data with Quick Sort..." << endl;
            timer.Start();
            int iterations = QuickSort(data, columns[sortOnChoice - 1]);
            cout << left << setw(15) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds" << " in " << iterations << " iterations." << endl << endl;
        } else if (sortChoice == 5) {
            cout << left << setw(15) << "BEGIN:" << "Sorting data with Merge Sort..." << endl;
            timer.Start();
            int iterations = MergeSort(data, columns[sortOnChoice - 1]);
            cout << left << setw(15) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds" << " in " << iterations << " iterations." << endl << endl;
        } else
            break;
        cout << endl << "Writing list out to \"output.txt\"..." << endl;


        // Output the sorted data
        ofstream output("output.txt");
        for (map<string, string>::iterator it = data[0].fields.begin();
                it != data[0].fields.end(); it++) {
            output << left << setw(30) << it->first;
        }

        output << endl;

        for (unsigned int i = 0; i < data.size(); i++) {
            data[i].Output(output);
        }
        output.close();

        cout << endl;
    }



    Menu::Header("Hit ENTER to quit.");

    cin.ignore();
    cin.get();

    return 0;
}
//@pre data is in .csv format and has a header row
//@post every non-header row will be stored in a DataEntry object as a map in the form map<header,value>

void ReadData(vector<DataEntry>& data, const string& filename) {
    ifstream input(filename);
    if (input.fail()) {
        throw FileNotFoundException("error loading file, aborting program");
    }

    vector<string> headerItems;

    unsigned int field = 0;

    string line;
    bool header = true;
    bool skippedGeoComma = false;
    while (getline(input, line)) {
        DataEntry entry;

        int columnBegin = 0;
        field = 0;
        skippedGeoComma = false;

        for (unsigned int i = 0; i < line.size(); i++) {
            //cout << line[i];
            if (line[i] == ',') {
                int length = i - columnBegin;
                string substring = line.substr(columnBegin, length);

                if (header) {
                    headerItems.push_back(substring);
                } else {
                    string fieldKey = "";
                    if (field >= headerItems.size()) {
                        fieldKey = "Unknown";
                    } else {
                        fieldKey = headerItems[field];
                    }
                    entry.fields[ fieldKey ] = substring;
                }

                columnBegin = i + 1;

                if (header == false && skippedGeoComma == false && headerItems[ field ] == "GeoLocation") {
                    skippedGeoComma = true;
                    // Ignore this comma.
                    continue;
                } else {
                    field++;
                }
            }
        }

        if (header) {
            header = false;
        } else {
            data.push_back(entry);
        }
    }

    input.close();
}

/*SelectionSort is a sorting algorithm 
@pre none
@post vector is sorted from least to greatest
@param data is a vector of DataEntry class type objects
@param onKey is a string member variable of the data object
@desc SelectionSort iterates through the vector from j to n-1, moving the least significant value
to the beginning of the vector, then starting over at the next index the process repeats.
iMin is the index where the first smallest item will be stored and then the next
in the "i" for loop, if the onKey value in the i index is less than the value in the iMin index
then iMin gets set to i, at which point the */
int SelectionSort(vector<DataEntry>& data, const string& onKey) {
	int n = data.size(), iterations = 0, iMin;
    string temp_key = onKey;
    for (int j = 0; j < n - 1; j++) {
        iMin = j;
        
		for (int i = j + 1; i < n; i++) {
			/*TestOutput(data, temp_key, i,j, iMin);
			Menu::Pause();*/
			if (data[i].fields[onKey].compare(data[iMin].fields[onKey]) < 0) {
				iMin = i;
				//TestOutput(data, temp_key, i,j, iMin);
				//Menu::Pause();
			}
		}
            
            if (iMin != j) {
                DataEntry temp = data[j];
                data[j] = data[iMin];
                data[iMin] = temp;
            }
           
			iterations++;
        
    }
	/*cout << " | ";
	for (vector<DataEntry>::iterator it = data.begin(); it != data.end(); it++)
	{
		cout << it->fields[onKey] << " | ";
	}
	cout << endl;*/
	return iterations;
}

int InsertionSort(vector<DataEntry>& data, const string& onKey) {
	int iterations = 0;
	int n = data.size(); //n = size of the vector
	for (int unsorted = 1; unsorted < n; unsorted++)
	{
		string nextItem = data[unsorted].fields[onKey]; // store the first value in the "unsorted" portion of the vector in nextItem
		int loc = unsorted; // initially set loc to the first unsorted index
		/*while the value in nextItem is smaller than each previous value in the sorted half, shift the value in the sorted half to the right and make room for nextItem*/
		while (loc > 0 && (data[loc - 1].fields[onKey] > nextItem))
		{
			data[loc].fields[onKey] = data[loc - 1].fields[onKey];
			loc--;
			iterations++;
		}
		data[loc].fields[onKey] = nextItem; // insert nextItem
	}
	return iterations;
}

int BubbleSort(vector<DataEntry>& data, const string& onKey) {
    int iterations = 0;

    int n = data.size();

    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (data[j].fields[onKey] > data[j + 1].fields[onKey]) {
                DataEntry temp = data[j];
                data[j] = data[j + 1];
                data[j + 1] = temp;
            }
            iterations++;
        }
    }

    return iterations;
}

int MergeSort(vector<DataEntry>& data, const string& onKey) {
    int iterations = 0;

    MergeSort_Recursive(data, 0, data.size() - 1, iterations, onKey);

    return iterations;
}

void MergeSort_Recursive(vector<DataEntry>& data, int first, int last, int& iterations, const string& onKey) {
    iterations++;

    if (first < last) {
        int mid = first + (last - first) / 2;

        MergeSort_Recursive(data, first, mid, iterations, onKey);
        MergeSort_Recursive(data, mid + 1, last, iterations, onKey);
        Merge(data, first, mid, last, iterations, onKey);
    }
}

void Merge(vector<DataEntry>& data, int first, int mid, int last, int& iterations, const string& onKey) {
    vector<DataEntry> temp = data;

    int first1 = first; //Beginning of first subvector
    int last1 = mid; // end of first subvector
    int first2 = mid + 1; // beginning of second subvector
    int last2 = last; // end of second subvector

    int index = first1; // next available location in subarray
    while ((first1 <= last1) && (first2 <= last2)) {
        if (data[first1].fields[onKey] <= data[first2].fields[onKey]) {
            temp[index] = data[first1];
            first1++;
        } else {
            temp[index] = data[first2];
            first2++;
        }
        index++;
        iterations++;
    }

    while (first1 <= last1) {
        temp[index] = data[first1];
        first1++;
        index++;
        iterations++;
    }

    while (first2 <= last2) {
        temp[index] = data[first2];
        first2++;
        index++;
        iterations++;
    }

    data = temp;
}

int QuickSort(vector<DataEntry>& data, const string& onKey) {
    int iterations = 0;

    QuickSort_Recursive(data, 0, data.size() - 1, iterations, onKey);

    return iterations;
}

void QuickSort_Recursive(vector<DataEntry>& data, int low, int high, int& iterations, const string& onKey) {
    iterations++;

    if (low < high) {
        int partitionIndex = Partition(data, low, high, iterations, onKey);

        QuickSort_Recursive(data, low, partitionIndex - 1, iterations, onKey);
        QuickSort_Recursive(data, partitionIndex + 1, high, iterations, onKey);
    }
}

int Partition(vector<DataEntry>& data, int low, int high, int& iterations, const string& onKey) {
    DataEntry pivot = data[high];

    int i = (low - 1);

    for (int j = low; j <= high - 1; j++) {
        if (data[j].fields[onKey] <= pivot.fields[onKey]) {

            i++;

            DataEntry temp = data[i];
            data[i] = data[j];
            data[j] = temp;
        }
        iterations++;
    }

    DataEntry temp = data[i + 1];
    data[i + 1] = data[high];
    data[high] = temp;

    return (i + 1);
}

void TestOutput(const vector<DataEntry>& data, const string& onKey, int idx_i, int idx_j, int min) {
    
    
    vector<DataEntry>::const_iterator di;
    DataEntry temp;
    int data_size = data.size();
    cout << " | ";
    int width, count = 0;
    string index;
    for(di = data.begin(); di != data.end(); di++)
    {
        if(count == idx_i)
            index = "i";
        else if(count == idx_j)
            index = "j";
        else
            index = ".";
        temp = *di;
        width = temp.fields[onKey].size();
        if(count == min)
            index += "*";
        cout << setw(width) << index << " | ";
        count++;
    }
    cout << endl;
    cout << " | ";
    for (di = data.begin(); di < data.end(); di++) {
        temp = *di;
        cout << temp.fields[onKey] << " | ";

    }
    cout << endl;
}


//int RadixSort(vector<DataEntry>& data, const string& onKey)
//{
//	int iterations = 0;
//
//	string m = GetMax(data, onKey);
//
//	for (int exp = 1; m. / exp > 0; exp *= 10)
//	{
//		CountSort(data, exp, iterations);
//	}
//
//	return iterations;
//}
//string GetMax(vector<DataEntry>& data, const string& onKey)
//{
//	string maxval = data[0].fields[onKey];
//	for (unsigned int i = 1; i < data.size(); i++)
//	{
//		if (data[i].fields[onKey] > maxval)
//		{
//			maxval = data[i].fields[onKey];
//		}
//	}
//
//	return maxval;
//}
//
//void CountSort(vector<DataEntry>& data, int exp, int& iterations)
//{
//	vector<DataEntry> output = data;
//	int count[10] = { 0 };
//
//	for (unsigned int i = 0; i < data.size(); i++)
//	{
//		count[(data[i] / exp) % 10]++;
//		iterations++;
//	}
//
//	for (int i = 1; i < 10; i++)
//	{
//		count[i] += count[i - 1];
//		iterations++;
//	}
//
//	for (int i = data.size() - 1; i >= 0; i--)
//	{
//		output[count[(data[i] / exp) % 10] - 1] = data[i];
//		count[(data[i] / exp) % 10]--;
//		iterations++;
//	}
//
//	data = output;
//}
