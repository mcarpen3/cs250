#include "Tester.hpp"
//template enabled tester, mainly for booleans and integers
template<class C>
void tester_class(C actual, C expected);

void Tester::RunTests() {
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();

    Test_ShiftRight();
    Test_ShiftLeft();

    Test_Remove();
    Test_Insert();
}

void Tester::DrawLine() {
    cout << endl;
    for (int i = 0; i < 80; i++) {
        cout << "-";
    }
    cout << endl;
}

void Tester::Test_Init() {
    DrawLine();
    cout << "TEST: Test_Init" << endl;
    List<int> listTest;
    if(listTest.IsEmpty())
        cout << "Passed" << endl;
}

void Tester::Test_ShiftRight() {
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl;
    {
        //test begin
        cout << "TEST 1: Test_ShiftRight on an empty List." << endl;
        List<int> testList;
        tester_class(testList.ShiftRight(0),false);
        //end test
    }
    {
        //test begin
        cout << "TEST 2: Test_ShiftRight on a half-full list," 
                << " check the boolean return and last integer shift." << endl;
        List<int> testList;
        for(int i = 0;i<50;i++)
        {
            testList.PushBack(i);
        }
        cout << "Boolean Test:" << endl;
        tester_class(testList.ShiftRight(24),true);
        testList.m_itemCount++; //shiftRight doesn't increment the counter
        cout << "Integer Test:" << endl;
        tester_class(*(testList.Get(50)),49);
        //test end
    }
    {
        //test begin
        cout << "TEST 3: Test_ShiftRight on a half-full list,"
                << "Check the boolean return and index + 1 value." << endl;
        List<int> testList;
        for(int i = 0;i<50;i++)
        {
            testList.PushBack(i);
        }
        cout << "Boolean Test: " << endl;
        tester_class(testList.ShiftRight(24),true);
        testList.m_itemCount++; //shiftLeft doesn't increment the counter
        cout << "Integer Test:" << endl;
        tester_class(*(testList.Get(25)),24);
        //test end
    }
}

void Tester::Test_ShiftLeft() {
    DrawLine();
    cout << "TEST: Test_ShiftLeft" << endl;

   
    {
        //test begin
        cout << "TEST 1: Test_ShiftLeft on an empty List." << endl;
        List<int> testList;
        tester_class(testList.ShiftLeft(0),false);
        //end test
    }
    {
        //test begin
        cout << "TEST 2: Test_ShiftLeft on a half-full list," 
                << " check the boolean return and last integer shift." << endl;
        List<int> testList;
        for(int i = 0;i<50;i++)
        {
            testList.PushBack(i);
        }
        cout << "Boolean Test:" << endl;
        tester_class(testList.ShiftLeft(24),true);
        cout << "Integer Test:" << endl;
        tester_class(*(testList.Get(48)),49);
        //test end
    }
    {
        //test begin
        cout << "TEST 3: Test_ShiftLeft on a half-full list,"
                << "Check the boolean return and index + 1 value." << endl;
        List<int> testList;
        for(int i = 0;i<50;i++)
        {
            testList.PushBack(i);
        }
        cout << "Boolean Test: " << endl;
        tester_class(testList.ShiftLeft(24),true);
        cout << "Integer Test:" << endl;
        tester_class(*(testList.Get(24)),25);
        //test end
    }
}

void Tester::Test_Size() {
    DrawLine();
    cout << "TEST: Test_Size" << endl;

    { // Test begin
        cout << endl << "Test 1" << endl;
        List<int> testList;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if (actualSize == expectedSize) {
            cout << "Pass" << endl;
        } else {
            cout << "Fail" << endl;
        }
    } // Test end

    { // Test begin
        cout << endl << "Test 2" << endl;
        List<int> testList;

        testList.PushBack(1);

        int expectedSize = 1;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if (actualSize == expectedSize) {
            cout << "Pass" << endl;
        } else {
            cout << "Fail" << endl;
        }
    } // Test end
}

void Tester::Test_IsEmpty() {
    DrawLine();
    cout << "TEST: Test_IsEmpty" << endl;
    {
        List<int> testList;
        bool expected = true;
        bool actual = testList.IsEmpty();

        cout << "Expected result: " << expected << endl;
        cout << "Actual result:   " << actual << endl;

        if (actual == expected) {
            cout << "Pass" << endl;
        } else {
            cout << "Fail" << endl;
        }
    } // Test end

    { // Test begin
        cout << endl << "Test 2" << endl;
        List<int> testList;

        testList.PushBack(1);

        bool expected = 1;
        bool actual = testList.Size();

        cout << "Expected result: " << expected << endl;
        cout << "Actual result:   " << actual << endl;

        if (actual == expected) {
            cout << "Pass" << endl;
        } else {
            cout << "Fail" << endl;
        }
    } // Test end
    // Put tests here
}

void Tester::Test_IsFull() {
    DrawLine();
    cout << "TEST: Test_IsFull" << endl;

    // Test begin

    cout << endl << "Test 1" << endl;
    List<int> testList;
    bool expected = 0;
    bool actual = testList.IsFull();

    cout << "Expected result: " << expected << endl;
    cout << "Actual result:   " << actual << endl;

    if (actual == expected) {
        cout << "Pass" << endl;
    } else {
        cout << "Fail" << endl;
    }
    // Test end

    { // Test begin
        cout << endl << "Test 2" << endl;
        List<int> testList;
        for (int i = 0; i < ARRAY_SIZE; i++) {
            testList.PushBack(i);
        }

        bool expected = true;
        bool actual = testList.IsFull();

        cout << "Expected result: " << expected << endl;
        cout << "Actual result:   " << actual << endl;

        if (actual == expected) {
            cout << "Pass" << endl;
        } else {
            cout << "Fail" << endl;
        }
    } // Test end
}

void Tester::Test_PushFront() {
    DrawLine();
    cout << "TEST: Test_PushFront" << endl;

    {//test begin
        cout << "Test 1: push one integer onto empty stack." << endl;
        List<int> testList;
        bool actual = testList.PushFront(0);
        bool expected = true;
        if (actual == expected)
            cout << "Passed" << endl;
        else
            cout << "Failed" << endl;
    }//test end

    {//test begin
        cout << "Test 2: push ARRAY_SIZE integers onto the array, "
                << "then try one more." << endl;
        List<int> testList;
        for (int i = 0; i < ARRAY_SIZE; i++)
            testList.PushFront(i);
        bool actual = testList.PushFront(100);
        bool expected = false;
        if (actual == expected)
            cout << "Passed" << endl;
        else
            cout << "Failed" << endl;
    }//test end

    {//test begin
        cout << "Test 3: push three integers to front of array, "
                << "check the front integer." << endl;
        List<int> testList;
        for (int i = 0; i < 3; i++) {
            testList.PushFront(i);
        }
        int expected = 2;
        int* actual = testList.GetFront();
        if (expected == *actual) {
            cout << "Pass" << endl;
        } else {
            cout << "Fail" << endl;
        }
    }//test end

    {//test begin
        cout << "Test 4: fill array and try one more, check the front " << endl;
        List<int> testList;
        for (int i = 0; i < ARRAY_SIZE + 1; i++) {
            testList.PushFront(i);
        }
        //testList shouldn't fill up past the ARRAY_SIZE
        int expected = 99;
        int actual = *testList.GetFront();

        if (expected == actual) {
            cout << "Pass" << endl;
        } else
            cout << "Fail" << endl;
    }//test ends
}

void Tester::Test_PushBack() {
    DrawLine();
    cout << "TEST: Test_PushBack" << endl;

    {//test begin
        cout << "Test 1: push one integer onto empty stack." << endl;
        List<int> testList;
        bool actual = testList.PushFront(0);
        bool expected = true;
        if (actual == expected)
            cout << "Passed" << endl;
        else
            cout << "Failed" << endl;
    }//test end

    {//test begin
        cout << "Test 2: push ARRAY_SIZE integers onto the array, "
                << "then try one more." << endl;
        List<int> testList;
        for (int i = 0; i < ARRAY_SIZE; i++)
            testList.PushFront(i);
        bool actual = testList.PushFront(100);
        bool expected = false;
        if (actual == expected)
            cout << "Passed" << endl;
        else
            cout << "Failed" << endl;
    }//test end
    {//begin test
        cout << "Test 3: push three integers to back of array, "
                << " and check the back integer." << endl;
        List<int> testList;
        for (int i = 0; i < 3; i++) {
            testList.PushBack(i);
        }
        int expected = 2;
        int actual = *(testList.GetBack());
        if (expected == actual) {
            cout << "Pass" << endl;
        } else {
            cout << "Fail" << endl;
        }
    }//test end

    {//test begin
        cout << "Test 4: fill array to capacity, then try one more PushBack() "
                << "and check the back integer." << endl;
        List<int> testList;
        for (int i = 0; i < ARRAY_SIZE + 1; i++) {
            testList.PushBack(i);
        }
        //testList shouldn't fill up past the ARRAY_SIZE
        int expected = 99;
        int actual = *testList.GetBack();

        tester_class(actual, expected);
    }//test ends
}

void Tester::Test_PopFront() {
    DrawLine();
    {//test start
        cout << "TEST: Test_PopFront, pop an empty list" << endl;
        List<int> testList;
        bool actual = testList.PopFront();
        bool expected = false;
        tester_class(actual, expected);
    }//test end

    {//test start
        cout << "TEST 2: Test_PopFront, pop a list with one item"
                << " at the front." << endl;
        List<int> testList;
        testList.PushFront(0);
        bool actual = testList.PopFront();
        bool expected = true;
        tester_class(actual, expected);
    }//test end

    {//test start
        cout << "TEST 3: Test_PopFront, pop the front of a list " <<
                "with two items and check the front." << endl;
        List<int> testList;
        testList.PushBack(0);
        testList.PushBack(1);
        cout << "Boolean Test:" << endl;
        bool actual = testList.PopFront();
        bool expected = true;
        tester_class(actual, expected);
        cout << "Index Value check:" << endl;
        tester_class(*(testList.GetFront()),1);
    }//test end

    
}

void Tester::Test_PopBack() {
    DrawLine();
    {//test start
        cout << "TEST: Test_PopBack" << endl;
        cout << "TEST 1: Test_PopBack, pop back an empty list" << endl;
        List<int> testList;
        bool actual = testList.PopBack();
        bool expected = false;
        tester_class(actual, expected);
    }//test end

    {//test start
        cout << "TEST 2: Test_PopBack, pop back a list with one item"
                << " at the back." << endl;
        List<int> testList;
        testList.PushBack(0);
        bool actual = testList.PopBack();
        bool expected = true;
        tester_class(actual, expected);
    }//test end

    {//test start
        cout << "TEST 3: Test_PopBack, pop the back of a half-full list " <<
                "then check the integer at the back." << endl;
        List<int> testList;
        for(int i = 0;i<50;i++)
        {
            testList.PushBack(i);
        }
        
        cout << "Boolean test: " << endl;
        bool actual = testList.PopBack();
        bool expected = true;
        tester_class(actual, expected);
        cout << "Integer test: " << endl;
        tester_class(*(testList.GetBack()),48);
    }//test end

    
}

void Tester::Test_Clear() {
    DrawLine();
    cout << "TEST: Test_Clear" << endl;
    {//test begin
        cout << "TEST 1 : Test_Clear -- clear an empty list" << endl;
        List<int> listTest;
        listTest.Clear();
        int actual = listTest.Size();
        int expected = 0;
        tester_class(actual, expected);
    }//test end

    {//test begin
        cout << "TEST 2 : Test_Clear -- clear a full list" << endl;
        List<int> listTest;
        for (int i = 0; i < ARRAY_SIZE; i++)
            listTest.PushFront(i);
        listTest.Clear();
        int actual = listTest.Size();
        int expected = 0;
        tester_class(actual, expected);
    }//test end

    {//test start
        cout << "TEST 3: Test_Clear -- clear a partial list" << endl;
        List<int> listTest;
        for (int i = 0; i < 50; i++)
            listTest.PushBack(i);
        listTest.Clear();
        int actual = listTest.Size();
        int expected = 0;
        tester_class(actual, expected);
    }//test ends
}

void Tester::Test_Get() {
    DrawLine();
    cout << "TEST: Test_Get" << endl;

    {//test start
        cout << "TEST 1: Test_Get: get index 0 from empty list." << endl;
        List<int> listTest;
        int* actual = listTest.Get(0);
        int* expected = nullptr;
        tester_class(actual, expected);
    }//test end
    {
        cout << "TEST 2: Test_Get: get index 0,1,2 after Push_Back 2,1,0" 
                << endl;
        List<int> listTest;
        for (int i = 2; i >= 0; i--) {
            listTest.PushFront(i);
        }
        for (int i = 0; i < 3; i++) {
            cout << "test2." << i << endl;
            tester_class(*(listTest.Get(i)), i);
        }
    }
}

void Tester::Test_GetFront() {
    DrawLine();
    cout << "TEST: Test_GetFront" << endl;

    {
        //test begin
        cout << "TEST 1: create empty list, " <<
                "test Get Front should return nullptr" << endl;
        List<int> listTest;
        if (!(listTest.GetFront() == nullptr))
            cout << "Failed" << endl;
        else
            cout << "Passed" << endl;
        //test end
    }
    {
        //test begin
        cout << "TEST 2: create a list with 0,1, and 2 in the first three"
                << " indexes. Get_Front() should return 0." << endl;
        List<int> listTest;
        for (int i = 0; i < 3; i++) {
            listTest.PushBack(i);
        }

        tester_class(*listTest.GetFront(), 0);
        //test end
    }
    {
        //test begin
        cout << "TEST 3: create a full list with 0 at the front,"
                << "then Get_Front() should return zero" << endl;
        List<int> listTest;
        for (int i = 0; i < ARRAY_SIZE; i++) {
            listTest.PushBack(i);
        }
        //get_front on a full list
        tester_class(*listTest.GetFront(), 0);
    }



}

void Tester::Test_GetBack() {
    DrawLine();
    cout << "TEST: Test_GetBack" << endl;


    {
        //test begin
        cout << "TEST 1: create empty list, " <<
                "test Get Back should return nullptr" << endl;
        List<int> listTest;
        if (!(listTest.GetBack() == nullptr))
            cout << "Failed" << endl;
        else
            cout << "Passed" << endl;
        //test end
    }
    {
        //test begin
        cout << "TEST 2: create a half-full list "
                << "then push_back with 0,1, and 2 in the last three"
                << " indexes. Get_Back() should return 2." << endl;
        List<int> listTest;
        for (int i = 0; i < 50; i++) {
            listTest.PushBack(i);
        }
        for(int i = 0;i<3;i++)
            listTest.PushBack(i);

        tester_class(*listTest.GetBack(), 2);
        //test end
    }
    {
        //test begin
        cout << "TEST 3: create a full list with 0 at the front,"
                << "then Get_Back() should return 99" << endl;
        List<int> listTest;
        for (int i = 0; i < ARRAY_SIZE; i++) {
            listTest.PushBack(i);
        }
        //get_front on a full list
        tester_class(*listTest.GetBack(), 99);
    }


}

void Tester::Test_GetCountOf() {
    DrawLine();
    cout << "TEST: Test_GetCountOf" << endl;

    {
        //begin test
        cout << "TEST 1: GetCountOf empty list should be 0" << endl;
        List<int> listTest;

        tester_class(listTest.GetCountOf(0), 0);
        //end test
    }
    {
        //begin test
        cout << "TEST 2: GetCountOf list of 30 integers 15 are the target, "
                << "GetCountOf(target); should return 15." << endl;
        List<int> listTest;
        for (int i = 0; i < 30; i++) {
            if (i % 2 == 0)
                listTest.PushFront(1);
            else
                listTest.PushFront(2);
        }
        tester_class(listTest.GetCountOf(1), 15);
        //end test


    }
    {
        //begin test
        cout << "TEST 3: GetCountOf list of 100 integers 10 are the target, "
                << "GetCountOf(target) should return 10." << endl;
        List<int> listTest;
        for (int i = 0; i < ARRAY_SIZE; i++) {
            if (i % 10 == 0)
                listTest.PushFront(1);
            else
                listTest.PushFront(2);

        }
        tester_class(listTest.GetCountOf(1), 10);
        //end test
    }
}

void Tester::Test_Contains() {
    DrawLine();
    cout << "TEST: Test_Contains" << endl;
    {
        //begin test
        cout << "TEST 1: Test_Contains on an empty list, should return false"
                << endl;
        List<int> listTest;
        tester_class(listTest.Contains(1), false);
        //end test
    }
    {
        cout << "TEST 2: Test_Contains on a full list containing the target, "
                << "Should return true." << endl;
        List<int> listTest;
        for (int i = 0; i < ARRAY_SIZE; i++) {
            if (i == 50)
                listTest.PushFront(50);
            else
                listTest.PushFront(0);
        }
        tester_class(listTest.Contains(50), true);
        //end test
    }
    {
        //begin test
        cout << "TEST 3: Test_Contains on a full list not containing the target,"
                << "Should return false." << endl;
        List<int> listTest;
        for (int i = 0; i < ARRAY_SIZE; i++) {
            listTest.PushFront(i);
        }
        tester_class(listTest.Contains(100), false);
        //end test
    }


}

void Tester::Test_Remove() {
    DrawLine();
    cout << "TEST: Test_Remove" << endl;
    {
        //begin test
        cout << "TEST 1: Test_Remove on index 0 of empty list, "
                << "should return false." << endl;
        List<char> listTest;

        tester_class(listTest.Remove(static_cast<char> (0)), false);
        //end test
    }
    {
        //begin test
        cout << "TEST 2: Test_Remove on index 10 of list with 20 items "
                << "in the list. Should return true.";
        List<char> listTest;
        for (int i = 97; i < 118; i++) {
            listTest.PushFront(static_cast<char> (i));
        }
        cout << "Boolean Test: " << endl;
        tester_class(listTest.Remove(static_cast<char> (97)), true);
        cout << "Integer Test: " << endl;
        tester_class(*(listTest.GetFront()),static_cast<char>(116));
        //end test
    }


}

void Tester::Test_Insert() {
    DrawLine();
    cout << "TEST: Test_Insert" << endl;
    {
        //begin test
        cout << "TEST 1: Test_Insert on an empty list, should return true."
                << endl;
        List<int> listTest;
        if(listTest.Insert(0, 5))
            cout << "Inserted integer 5 at m_arr[0]";
        tester_class(*(listTest.Get(0)), 5);
        //end test
    }
    {
        //begin test
        cout << "TEST 2: Test_insert on a full list, should return false"
                << endl;
        List<int> listTest;
        for (int i = 0; i < ARRAY_SIZE; i++) {
            listTest.PushFront(i);
        }
        tester_class(listTest.Insert(0, 5), false);
        //end test
    }
    {
        //begin test
        cout << "TEST 3: Test_insert on a partial list, should return true"
                << endl;
        List<int> listTest;
        for (int i = 0; i < ARRAY_SIZE / 2; i++) {
            listTest.PushFront(i);
        }
        tester_class(listTest.Insert(4, 5), true);
    }

}

template<class C>
void tester_class(C actual, C expected) {
    if (actual == expected)
        cout << "Passed" << endl;
    else
        cout << "Failed" << endl;
}