#ifndef _LIST_HPP
#define _LIST_HPP

const int ARRAY_SIZE = 100;

template <typename T>
class List {
private:
    // private member variables
    int m_itemCount;
    T m_arr[ARRAY_SIZE];

    // functions for internal-workings

    /*precondition: atIndex should be the index where insertion will take place
     
     postcondition: array indexes will be shifted to m_itemCount, starting at
     the index, leaving duplicate values at the index and index + 1
     no counter increment handling*/

    bool ShiftRight(int atIndex) {
        if (!IsFull() && atIndex >= 0 && atIndex < m_itemCount) {
            for (int i = m_itemCount; i > atIndex; i--) {
                m_arr[i] = m_arr[i - 1];
            }

            return true;
        }
        return false;
    }

    /*precondition: atIndex should be the index where removal will take place
     
     postcondition: array index values from the index to item count - 1 will be
     shifted toward the index, overwriting the value stored at atIndex
     no counter increment handling*/

    bool ShiftLeft(int atIndex) {
        if (!IsEmpty() && atIndex >= 0 && atIndex < m_itemCount) {
            for (int i = atIndex; i < m_itemCount; i++) {
                m_arr[i] = m_arr[i + 1];
            }

            return true;
        }
        return false;
    }

public:

    List() : m_itemCount(0) {

    }

    ~List() {
    }

    // Core functionality

    int Size() const {
        return m_itemCount;
    }

    bool IsEmpty() const {
        return m_itemCount == 0;
    }

    bool IsFull() const {
        return m_itemCount == ARRAY_SIZE;
    }

    bool PushFront(const T& newItem) {

        if (IsEmpty()) {
            m_arr[0] = newItem; //empty array 0th index gets newItem 
            m_itemCount++;
            return true;
        }
        if (ShiftRight(0)) {
            m_arr[0] = newItem; //shift and push
            m_itemCount++;
            return true;
        }
        return false;//full array 
    }

    bool PushBack(const T& newItem) {
        if (!IsFull()) {
            m_arr[m_itemCount] = newItem;
            m_itemCount++;
            return true;
        }
        return false; //full array
    }

    bool Insert(int atIndex, const T& item) {
        if(IsEmpty()&& atIndex==0)
        {
            m_arr[atIndex] = item;
        }
        if(ShiftRight(atIndex))
        {
            m_arr[atIndex] = item;
            m_itemCount++;
            return true;
        }

        return false; //array is full or atIndex is non-contiguous
    }

    bool PopFront() {
        if (ShiftLeft(0)) {

            m_itemCount--;
            return true;
        }

        return false;
    }

    bool PopBack() {
        if (!IsEmpty()) {
            m_itemCount--;
            return true;
        }
        return false; //array was empty
    }

    bool Remove(const T& item) {
        if (Contains(item)) {
            int index = 0;
            while (true) {
                if (m_arr[index] = item) {
                    ShiftLeft(index);
                    return true;
                }

            }
        }
        return false;
    }

    bool Remove(int atIndex) {
        return false;
    }

    void Clear() {
        m_itemCount = 0;
    }

    // Accessors

    T* Get(int atIndex) {
        if(atIndex == 0 && IsEmpty())
           return &m_arr[atIndex]; 
        if (atIndex >= 0 && atIndex < m_itemCount)
            return &m_arr[atIndex];
        
        return nullptr; // return nullptr if out of bounds or index is empty; 
    }

    T* GetFront() {
        if (!IsEmpty())
            return &m_arr[0];
        return nullptr; //return nullptr if list is empty

    }

    T* GetBack() {
        if (!IsEmpty())
            return &m_arr[m_itemCount - 1]; //return the last item in the list
        return nullptr; //return nullptr if list is empty

    }

    // Additional functionality

    int GetCountOf(const T& item) const {
        int count = 0;
        for (int i = 0; i < m_itemCount; i++)
            if (m_arr[i] == item)
                count++; //keep count of item instances

        return count;
    }

    bool Contains(const T& item) const {
        if (!IsEmpty()) {
            for (int i = 0; i < m_itemCount; i++)
                if (m_arr[i] == item)
                    return true;

        }
        return false; //item doesn't exist in the array
    }

    friend class Tester;
};


#endif
